import dotenvx from '@dotenvx/dotenvx'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

const env = dotenvx.config({
    path: process.env.NODE_ENV === 'production' ? '.env.production' : '.env.development'
}).parsed

export default defineConfig({
    plugins: [react()],
    base: '/lms',
    define: {
        'process.env': env,
    },
    build: {
        manifest: true,
        emptyOutDir: true,
        rollupOptions: {
            output: {
                entryFileNames: 'js/[name]-[hash].js',
                chunkFileNames: 'chunk/[name]-[hash].js',
                assetFileNames: ({ name }) => {
                    if (/\.(gif|jpe?g|png|svg)$/.test(name ?? ''))
                        return 'img/[name]-[hash][extname]'
                    else if (/\.css$/.test(name ?? ''))
                        return 'css/[name]-[hash][extname]'
                    else if (/\.ico$/.test(name ?? ''))
                        return '[name]-[hash][extname]'
                    else return 'assets/[name]-[hash][extname]'
                },
            }
        }
    },
})
