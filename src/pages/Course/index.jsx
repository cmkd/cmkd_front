import * as React from 'react'
import {useLocation, useNavigate} from 'react-router-dom'
import axios from '../../utils/axios'
import NestedList from './components/NestedList'
import List from '@mui/material/List'
import {Box, Grid, Typography, Button} from '@mui/material'
import { dfp } from '../../utils/dfp'

export default function () {
    let location = useLocation()
    let navigate = useNavigate()

    const [edelements, setEdelements] = React.useState()
    const [userEducationCourse, setUserEducationCourse] = React.useState()
    const [currentEdelement, setCurrentEdelement] = React.useState(null)
    React.useEffect(() => {
        let edelement
        async function usereducationcourse() {
            await axios
                .get(`/api/user/course/${location.state.course_id}`, {})
                .then(({data}) => {
                    setUserEducationCourse(data)

                    edelement = data.educationcourse.edelement_id
                })
            await axios
                .get(
                    `/api/edelement/${edelement}/children?edelement=true&level=2`,
                    {}
                )
                .then(({data}) => {
                    setEdelements(data)
                })
        }
        usereducationcourse()
    }, [])

    React.useEffect(() => {
        if (edelements) {
            let current
            if (userEducationCourse.current_edelement_id == null) current = 1
            else current = userEducationCourse.current_edelement_id
            edelements.children.forEach((theme) => {
                if (theme.children_count > 0) {
                    theme.children.forEach((element) => {
                        if (
                            (current == 1 &&
                                element.didacticdescription.packnumber == 1) ||
                            element.id === current
                        )
                            current = element
                    })
                }
            })

            setCurrentEdelement(current)
        }
    }, [edelements])

    const handleLastElement = () => {
        dfp(
            location.state.usereducationprofile_id,
            'Курс по дисциплине',
            'Лекция по ' + currentEdelement.shortname,
            'Кнопка "Продолжить"',
            'Нажатие',
            'Учащийся продолжает работу с курсом, переходя сразу к последнему доступному материалу' +
            ' edelement_id: ' + currentEdelement.id,
        )
        navigate(`/lms/read/`, {
            state: {
                edelement: currentEdelement,
                course_id: location.state.course_id,
                edelements: edelements.children,
                usereducationprofile_id:
                    location.state
                        .usereducationprofile_id,
                currentEdelement: currentEdelement,
                plan_id: location.state.plan_id,
            },
        })
    }

    if (edelements)
        return (
            <Grid container>
                <Grid item xs={12} textAlign={'center'} m={2}>
                    <Typography>
                        {userEducationCourse.educationcourse.name}
                    </Typography>
                </Grid>
                <Grid item xs={12} textAlign={'justify'} m={2}>
                    <Typography>
                        {userEducationCourse.educationcourse.about}
                    </Typography>
                </Grid>
                {userEducationCourse.current_edelement_id == null ? (
                    <Grid item xs={12} textAlign={'justify'} m={2}>
                        <Typography>
                            Приветствуем вас! Для начала работы с данным курсом
                            выберите любой дидактический материал из списка
                            ниже. Элементами первого уровня являются темы,
                            внутри них представлены целевые элементы вашего
                            обучения - дидактические материалы, изучение которых
                            позволит вам освоить данную дисциплину. По нажатию
                            на любой из них вы будете переадрессованы на
                            материал вводного занятия. В дальнейшем вы сможете
                            обратиться к ранее изученному материалу в любой
                            момент.
                        </Typography>
                    </Grid>
                ) : (
                    <>
                        <Grid item xs={7} textAlign={'justify'} m={2}>
                            <Typography>
                                С возвращением! Для продолжения работы с курсом
                                выберите интересующий вас дидактический материал
                                либо нажмите на кнопку рядом.
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Button
                                fullWidth
                                variant='contained'
                                sx={{m: 2, minWidth: 120}}
                                onClick={handleLastElement}>
                                Продолжить работу
                            </Button>
                        </Grid>
                    </>
                )}
                <Grid item xs={12}>
                    <Typography></Typography>
                </Grid>
                <Grid item xs={12}>
                    <List
                        sx={{
                            width: '100%',
                            maxWidth: 360,
                            bgcolor: 'background.paper',
                        }}
                        component='nav'
                        aria-labelledby='nested-list-subheader'>
                        <NestedList
                            listItems={edelements.children}
                            all={edelements.children}
                            current={{
                                inUED: userEducationCourse.current_edelement_id,
                                object: currentEdelement,
                            }}
                        />
                    </List>
                </Grid>
            </Grid>
        )
}
