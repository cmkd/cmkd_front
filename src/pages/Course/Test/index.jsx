import * as React from 'react'
import {useLocation, useNavigate} from 'react-router-dom'
import axios from '../../../utils/axios'
import {
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    Typography,
    Checkbox,
    Button,
    Grid,
} from '@mui/material'
import { dfp } from '../../../utils/dfp'

export default function () {
    let location = useLocation()
    let navigate = useNavigate()
    let startTime = null

    const [test, setTest] = React.useState()
    const [result, setResult] = React.useState({
        currentQuest: 0,
        results: [],
        score: 0,
        timer: 0,
        text: '',
        url: false,
    })
    const [currentResult, setCurrentResult] = React.useState([])
    const [completed, setCompleted] = React.useState(false)

    const [clicked, setClicked] = React.useState()
    const [clickCount, setClickCount] = React.useState(0)

    // Загрузка тестового бланка с вопросами, относящимися к единице материала
    React.useEffect(() => {
        console.log(location.state)
        async function test() {
            await axios
                .get(
                    `/api/test/${location.state.test_id}?edelement=${location.state.edelement_id}`,
                    {}
                )
                .then(({data}) => {
                    console.log(data)
                    setTest(data)
                    startTime = performance.now()
                })
        }
        test()
    }, [])

    // Завершение теста, запись результата, редактирование образовательного курса, запись ОЦС
    React.useEffect(() => {
        async function complete() {
            await axios
                .post(`/api/test/${location.state.test_id}/complete`, {
                    timer: result.timer,
                    score: result.score.toFixed(1),
                    results: result.results,
                    position: location.state.position
                        ? location.state.position
                        : null,
                })
                .then(({data}) => {
                    console.log(data)
                    if (
                        data.result &&
                        test.option != 0 &&
                        location.state.nextEdelement == undefined
                    ) {
                        axios.put(
                            `/api/user/course/${location.state.course_id}?score=1`
                        )
                    }
                    if (data.result && location.state.nextEdelement) {
                        axios.put(
                            `/api/user/course/${location.state.course_id}?current_edelement_id=${location.state.nextEdelement.id}`
                        )
                    }
                    const edelement = location.state.edelement
                    const currentPosition = edelement ? 'Тест по ' + edelement.shortname : 'Анкетирование'
            
                    const params = 
                    'время: ' + result.timer + 
                    ', процент: ' + result.score.toFixed(1)
                    dfp(
                        location.state.usereducationprofile_id, 
                        currentPosition,
                        'Обзор результатов',
                        'Далее',
                        'Тест пройден',
                        params,
                    )
                })
        }
        if (completed) complete()
    }, [completed])

    // Запись ОЦС при выборе ответа
    React.useEffect(() => {
        if (test){
            let answer, quest
            quest = test.quests.find(a => a.id == clicked.quest_id)
            answer = quest.answers.find(a => a.id == clicked.answer_id)
            if (quest && answer) {
                setClickCount(clickCount + 1)
                const edelement = location.state.edelement
                const edelement_id = edelement ? edelement.id : null
                const currentPosition = edelement ? 'Тест по ' + edelement.shortname : 'Анкетирование'
                const params = 
                    '{usereducationcourse_id : ' + location.state.usereducationprofile_id +
                    ', edelement_id: ' + edelement_id +
                    ', quest_name: ' + quest.name +
                    ', answer_name: ' + answer.name + '}'
                /*dfp(
                    location.state.usereducationprofile_id, 
                    currentPosition,
                    '-',
                    'Вариант ответа',
                    'Выбор',
                    params,
                )*/
            }
            
        }
    },[clicked])

    // Переход к следующему вопросу, запись ОЦС

    const dfpOnReply = () => {
        const edelement = location.state.edelement
        const currentPosition = edelement ? 'Тест по ' + edelement.shortname : 'Анкетирование'

        dfp(
            location.state.usereducationprofile_id, 
            currentPosition,
            '-',
            'Ответ на вопрос',
            'переход к следующему',
            'Кликов по ответам: ' + clickCount,
        )
    }

    const handleReply = () => {
        
        setClickCount(0)
        let newResults = result.results
        // Обработка вопроса с множеством вариантов ответа
        if (
            test.quests[result.currentQuest].quest_type == 0
        ) {
            let cr = []
            currentResult.forEach((element) => {
                let t = {
                    quest_id: element.quest_id,
                    answer_id: element.answer_id,
                    value: element.value,
                }
                cr.push(t)
            })
            newResults = newResults.concat(cr)
        } 
        // Отработка вопроса с одним вариантом ответа
        else {
            newResults = newResults.concat(currentResult)
        }

        // Пропуск вопроса с ползунками для ответов
        if (
            test.quests.length != result.currentQuest + 1 &&
            test.quests[result.currentQuest + 1]
                .quest_type == 4
        ){
            if ( test.quests.length > result.currentQuest + 2) {
                setResult((prev) => ({
                    ...prev,
                    currentQuest: result.currentQuest + 2,
                    results: newResults,
                }))
                dfpOnReply()
            } 
            else {
                computeResult(newResults)
            }
        }
        /** */

        if (test.quests.length == result.currentQuest + 1) {
            computeResult(newResults)
        } 
        else {
            setResult((prev) => ({
                ...prev,
                currentQuest: result.currentQuest + 1,
                results: newResults,
            }))
            dfpOnReply()
        }
        setCurrentResult([])
        console.log(clickCount, result.results)
    }

    const computeResult = (newResults) => {
        const endTime = performance.now()
        const elapsedSeconds =
            (endTime - startTime) / 1000
        let score = 0
        const questIds = []
        newResults.forEach((res) => {
            score += res.value
            if (res.value == 0) questIds.push(res.answer_id)
            questIds.push(res.quest_id)
        })
        score = (score / [... new Set(questIds)].length) * 100
        let t = ''
        if (score < 45) t = 'Плохо'
        else if (score < 75) t = 'Хорошо'
        else if (score >= 75) t = 'Очень хорошо'
        let url = false
        if (location.state.edelement && location.state.edelement.level == 4){
            if (score < 45) url = true
        }
        setCompleted(true)
        setResult({
            score: score,
            timer: elapsedSeconds,
            currentQuest: result.currentQuest + 1,
            results: newResults,
            text: t,
            url: url,
        })
        dfpOnReply()
    }

    const nextLocation = (recommended) => {
        const edelement = location.state.edelement
        const testEnding = edelement 
            ? 'Тест по ' + edelement.name + ' пройден ' + result.text + ', на ' + result.score + '%. ' 
            : 'Состоялось анкетирование'
        
        if (recommended){
            dfp(
                location.state.usereducationprofile_id, 
                'Результаты теста',
                'Лекция по ' + edelement.shortname,
                'Вернуться к материалу',
                'Ознакомление с результатом окончено, Далее по рекомендации',
                testEnding + 'Учащийся последовал рекомендации изучить материал повторно.',
            )
            navigate(`/lms/read/`, {
                state: {
                    edelement: edelement,
                    course_id: location.state.course_id,
                    edelements: location.state.edelements,
                    usereducationprofile_id: location.state.usereducationprofile_id,
                    currentEdelement: location.state.nextEdelement,
                    plan_id: location.state.plan_id,
                },
            })
        }
        else if (location.state.nextEdelement) {
            /*if (result.score < 100)
                navigate(`/lms/test/after`, {
                    state: {
                        nextEdelement:
                            location.state.nextEdelement,
                        course_id: location.state.course_id,
                        edelements:
                            location.state.edelements,
                        usereducationprofile_id:
                            location.state
                                .usereducationprofile_id,
                        currentEdelement:
                            location.state.nextEdelement,
                        position: location.state.position,
                        test_id: location.state.test_id,
                        cmkd_edelement_id:
                            location.state.edelements[0]
                                .parent_id,
                        plan_id: location.state.plan_id,
                    },
                })
            else*/
                
                dfp(
                    location.state.usereducationprofile_id, 
                    'Результаты теста',
                    'Лекция по ' + location.state.nextEdelement.shortname,
                    'Далее',
                    'Ознакомление с результатом окончено, далее по курсу',
                    testEnding,
                )
                navigate(`/lms/read/`, {
                    state: {
                        edelement: location.state.nextEdelement,
                        course_id: location.state.course_id,
                        edelements: location.state.edelements,
                        usereducationprofile_id: location.state.usereducationprofile_id,
                        currentEdelement: location.state.nextEdelement,
                        plan_id: location.state.plan_id,
                    },
                })
        } else {
                dfp(
                    location.state.usereducationprofile_id, 
                    'Результаты теста',
                    'Главная LMS',
                    'Далее в оглавление LMS',
                    'Ознакомлен с результатом',
                    testEnding,
                )
            navigate(`/lms`, {
                state: {
                    course_id: location.state.course_id,
                    usereducationprofile_id: location.state.usereducationprofile_id,
                },
            })
        }

        
    }

    if (test != undefined && !completed)
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Typography>
                        {result.currentQuest + 1} из {test.quests.length}
                    </Typography>
                    <Typography>
                        {test.quests[result.currentQuest].name}
                    </Typography>
                </Grid>

                {(test.quests[result.currentQuest].quest_type == 1 ||
                    test.quests[result.currentQuest].quest_type == 2) && (
                    <Grid item xs={12}>
                        <TestRadioGroup
                            quest_id={test.quests[result.currentQuest].id}
                            answers={test.quests[result.currentQuest].answers}
                            setValue={setCurrentResult}
                            value={currentResult}
                            setClicked={setClicked}
                        />
                    </Grid>
                )}
                {test.quests[result.currentQuest].quest_type == 0 && (
                    <Grid item xs={12}>
                        <TestCheckboxGroup
                            quest_id={test.quests[result.currentQuest].id}
                            answers={test.quests[result.currentQuest].answers}
                            setValue={setCurrentResult}
                            value={currentResult}
                            setClicked={setClicked}
                        />
                    </Grid>
                )}
                <Grid item xs={12}>
                    <Button
                        disabled={currentResult.length == 0}
                        variant='contained'
                        onClick={handleReply}>
                        Ответить
                    </Button>
                </Grid>
            </Grid>
        )
    else if (test != undefined && completed)
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Typography>Тест пройден</Typography>
                </Grid>
                {location.state.edelement && <>
                    <Grid item xs={12}>
                        <Typography>
                            Процент правильных ответов: {result.score.toFixed(1)}
                        </Typography>
                        <Typography>
                            Общая оценка: {result.text}
                        </Typography>
                    </Grid>

                    {result.url && 
                        <>
                            <Grid item xs={6}>
                                <Typography>
                                    Рекомендуем заново изучить материал
                                </Typography>
                                
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    variant='contained'
                                    onClick={() => nextLocation(true)}>
                                    Вернуться
                                </Button>
                            </Grid>
                        </>
                    }
                </>}
                

                <Grid item xs={12}>
                    <Button
                        variant='contained'
                        onClick={() => nextLocation(false)}>
                        Далее
                    </Button>
                </Grid>
            </Grid>
        )
}

function TestRadioGroup({quest_id, answers, setValue, value, setClicked}) {
    const handleChange = (event) => {
        const selectedAnswer = answers.find((a) => a.id == event.target.value)
        setValue({
            quest_id: quest_id,
            answer_id: selectedAnswer.id,
            value: selectedAnswer.value,
        })
        setClicked({
            quest_id: quest_id,
            answer_id: selectedAnswer.id,
            value: selectedAnswer.value,
        })
    }

    return (
        <FormControl sx={{m: 2}}>
            <RadioGroup value={value.answer_id || ''} onChange={handleChange}>
                {answers.map((elem) => (
                    <FormControlLabel
                        key={elem.id}
                        value={elem.id}
                        control={<Radio />}
                        label={elem.name}
                    />
                ))}
            </RadioGroup>
        </FormControl>
    )
}

function TestCheckboxGroup({quest_id, answers, value, setValue, setClicked}) {
    const handleCheckboxChange = (item) => {
        setClicked(item)
        let newCheckedItems
        //Снятие выбранного ответа
        const answeredBefore = value.some((val) => {return val.answer_id == item.answer_id})
        if (answeredBefore) {
            newCheckedItems = value.filter(
                (i) => i.answer_id !== item.answer_id
            )
        }
        //Включение ранее невыбранного ответа 
        else {
            newCheckedItems = [...value, item]
        }
        // Выбор варианта "Затрудняюсь ответить"
        const answerDifficulties = 
            ((item.prime == 1 && value.some((val) => { return val.prime == null })) 
            ||
            (item.prime == null && value.some((val) => { return val.prime == 1 })))

        if (answerDifficulties) {
            newCheckedItems = [item]
        }
        setValue(newCheckedItems)
    }

    return (
        <FormControl>
            {answers.map((item) => (
                <FormControlLabel
                    key={item.id}
                    control={
                        <Checkbox
                            checked={value.some((val) => {
                                return val.answer_id == item.id
                            })}
                            onChange={() =>
                                handleCheckboxChange({
                                    quest_id: quest_id,
                                    answer_id: item.id,
                                    value: item.value,
                                    prime: item.prime,
                                })
                            }
                            name={item.name}
                        />
                    }
                    label={item.name}
                />
            ))}
        </FormControl>
    )
}
