import * as React from 'react'
import {useLocation, useNavigate} from 'react-router-dom'
import axios from '../../../utils/axios'
import {Document, Page, pdfjs} from 'react-pdf'
import 'react-pdf/dist/esm/Page/TextLayer.css'
import 'react-pdf/dist/esm/Page/AnnotationLayer.css'
import {Grid, Typography, Tooltip, IconButton, Button} from '@mui/material'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import { dfp } from '../../../utils/dfp'

export default function () {
    pdfjs.GlobalWorkerOptions.workerSrc = `https://unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.mjs`
    const onDocumentLoadSuccess = ({numPages}) => {
        setTotalPages(numPages)
    }
    const [totalPages, setTotalPages] = React.useState(1)
    const [pageNumber, setPageNumber] = React.useState(1)
    const [prevElement, setPrevElement] = React.useState(null)
    const [nextElement, setNextElement] = React.useState(null)

    let location = useLocation()
    const navigate = useNavigate()

    const [pdf, setPDF] = React.useState(null)
    React.useEffect(() => {
        async function file() {
            await axios
                .get(
                    `/api/filestorage/${location.state.edelement.didacticdescription.filestorage_id}`,
                    {
                        responseType: 'blob',
                        transformResponse: [
                            function (data) {
                                let blob = new window.Blob([data], {
                                    type: 'application/pdf',
                                })
                                return window.URL.createObjectURL(blob)
                            },
                        ],
                    }
                )
                .then((data) => {
                    setPDF(data.data)
                })
        }
        if (location.state == null) {
            navigate('/lms/home')
        } else if (
            location.state.edelement.didacticdescription.filestorage_id != null
        )
            file()
        if (prevElement == null && nextElement == null) {
            let prev = null
            let next = null
            location.state.edelements.forEach((theme) => {
                if (theme.children_count > 0)
                    theme.children.forEach((element) => {
                        if (
                            element.didacticdescription.packnumber ==
                            location.state.edelement.didacticdescription
                                .packnumber -
                                1
                        )
                            prev = element
                        if (
                            element.didacticdescription.packnumber ==
                            location.state.edelement.didacticdescription
                                .packnumber +
                                1
                        )
                            next = element
                    })
            })
            setPrevElement(prev)
            setNextElement(next)
        }
    }, [location])

    const movePreviousPage = () => {
        const params = '{usereducationcourse_id : ' +
                    location.state
                        .usereducationprofile_id +
                    ', edelement_id: ' +
                    location.state.edelement.id +
                    '}'
        dfp(location.state.usereducationprofile_id, 
            'Лекция по ' + location.state.edelement.shortname,
            '-',
            'pdf',
            'Прошлая страница',
            params,
        )
        setPageNumber(pageNumber - 1)
    }

    const moveNextPage = () => {
        const params = '{usereducationcourse_id : ' +
                    location.state
                        .usereducationprofile_id +
                    ', edelement_id: ' +
                    location.state.edelement.id +
                    '}'
        dfp(
            location.state.usereducationprofile_id,
            'Лекция по ' + location.state.edelement.shortname,
            '-',
            'pdf',
            'Следующая страница',
            params,
        )
        setPageNumber(pageNumber + 1)
    }

    if (pdf != null)
        return (
            <Grid container>
                <PrevElement
                    prevElement={prevElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
                <NextElement
                    nextElement={nextElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
                

                <GetBack location={location} navigate={navigate} />
                <Grid item xs={11} textAlign={'center'}>
                    <Typography variant='h5'>
                        #{location.state.edelement.didacticdescription.packnumber}
                        - {location.state.edelement.name}
                    </Typography>
                </Grid>

                <Grid item xs={12}>
                    <Document
                        file={{url: pdf}}
                        onClick={() =>
                            console.log('Произведено нажатие на документ')
                        }
                        onLoadSuccess={onDocumentLoadSuccess}>
                        <Page scale={1.4} size='A4' pageNumber={pageNumber} />
                    </Document>
                </Grid>

                <Grid item xs={4} textAlign={'right'}>
                    {pageNumber !== 1 &&
                        <Tooltip title='Прошлая страница'>
                            <IconButton
                                aria-label='settings'
                                onClick={movePreviousPage}
                            >
                                <KeyboardArrowLeftIcon
                                    sx={{fontSize: 40}}
                                    color='success'
                                />
                            </IconButton>
                        </Tooltip>
                    }
                </Grid>
                <Grid item xs={4} textAlign={'center'}>
                    <Typography>
                        Страница {pageNumber} из {totalPages}
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    {pageNumber !== totalPages &&
                        <Tooltip title='Следующая страница'>
                            <IconButton
                                aria-label='settings'
                                onClick={moveNextPage}
                            >
                                <KeyboardArrowRightIcon
                                    sx={{fontSize: 40}}
                                    color='success'
                                />
                            </IconButton>
                        </Tooltip>
                    }
                </Grid>

                <PrevElement
                    prevElement={prevElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
                <NextElement
                    nextElement={nextElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
            </Grid>
        )
    else
        return (
            <Grid container>
                <PrevElement
                    prevElement={prevElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
                <NextElement
                    nextElement={nextElement}
                    location={location}
                    setNextElement={setNextElement}
                    setPageNumber={setPageNumber}
                    setPrevElement={setPrevElement}
                    setPDF={setPDF}
                    navigate={navigate}
                />
                <GetBack location={location} navigate={navigate} />
                <Grid item xs={11} textAlign={'center'}>
                    <Typography variant='h5'>
                        #
                        {
                            location.state.edelement.didacticdescription
                                .packnumber
                        }{' '}
                        - {location.state.edelement.name}
                    </Typography>
                </Grid>
                <Grid item xs={12} textAlign={'center'}>
                    <Typography>
                        Для данного параграфа не предусмотрен электронный
                        материал - материал предоставляется очно на лекции.
                        Проверьте у вас наличие конспекта по нему и двигайтесь
                        дальше.
                    </Typography>
                </Grid>
            </Grid>
        )
}

function NextElement({
    nextElement,
    location,
    setPrevElement,
    setNextElement,
    setPageNumber,
    setPDF,
    navigate,
}) {

    const currentEdelement = location.state.currentEdelement
    const edelement = location.state.edelement
    const edelements = location.state.edelements
    console.log(edelement)
    const test_type = edelement.didacticdescription.test_type
    let testEdelement_id = edelement.id
    let notLastElement = false
    let title
    if (nextElement)
    title = 'Следующий материал - ' + nextElement.name
    if (currentEdelement == edelement){
        const theme = edelements.find(a => a.id == edelement.parent_id)
        const discipline = theme.parent_id
        console.log(theme.id, discipline)
        if (test_type == 'u') title = '-> Вопросы для самопроверки'
        if (edelement.quests.length == 0 && nextElement){
            title = 'Вопросы не предусмотрены, далее - ' + nextElement.name
            testEdelement_id = null
        } 
        if (test_type == 't') {
            title = '-> Вопросы по теме ' + theme.name
            testEdelement_id = theme.id
        }
        if (test_type == 'd'){
            title = '-> Общий тест по дисциплине'
            testEdelement_id = discipline
        } 
    }
    else {
        testEdelement_id = null
        notLastElement = true
    }

    async function usereducationcourse() {
        await axios
            .put(
                `/api/user/course/${location.state.course_id}?current_edelement_id=${nextElement.id}`,
                {}
            )
            .then(({data}) => {
                console.log(data)
            })
    }

    async function test(edelement_id) {
        await axios
            .get(
                `api/discipline/${location.state.currentEdelement.discipline_id}/tests?option=1`,
                {}
            )
            .then(({data}) => {
                navigate(`/lms/test`, {
                    state: {
                        course_id: location.state.course_id,
                        usereducationprofile_id:
                            location.state
                                .usereducationprofile_id,
                        test_id: data.id,
                        edelement_id: edelement_id,
                        edelement: location.state.edelement,
                        position:
                            location.state
                                .currentEdelement
                                .didacticdescription
                                .packnumber,
                        nextEdelement: nextElement ? nextElement : null,
                        edelements: location.state.edelements,
                        plan_id: location.state.plan_id,
                    },
                })
            })
    }

    const handleClickNext = () => {
        const params = '{usereducationcourse_id : ' +
            location.state.usereducationprofile_id +
            ', edelement_id: ' +
            location.state.edelement.id +
            '}'
        //переход на тест - последний пройденный элемент ниже по позиции, чем следующий, для текущего существует сценарий теста
        if (testEdelement_id) {
            dfp(
                location.state.usereducationprofile_id,
                'Лекция по ' + location.state.edelement.shortname,
                'Тест',
                'Кнопка перехода',
                'Движение вперёд',
                params,
            )
            test(testEdelement_id)
        }
        //переход на следующий элемент - последний пройденный элемент ниже по позиции, чем следующий, у текущего нет вопросов для прохождения
        else if (
            !notLastElement && !testEdelement_id
        ) {
            usereducationcourse()
            dfp(
                location.state.usereducationprofile_id,
                'Лекция по ' + location.state.edelement.shortname,
                'Лекция по ' + nextElement.shortname,
                'Кнопка перехода',
                'Движение вперёд',
                params,
            )
            navigate(`/lms/read/`, {
                state: {
                    edelement: nextElement,
                    course_id: location.state.course_id,
                    edelements: location.state.edelements,
                    usereducationprofile_id:
                        location.state
                            .usereducationprofile_id,
                    currentEdelement: nextElement,
                    plan_id: location.state.plan_id,
                },
            })
            setPrevElement(null)
            setNextElement(null)
            setPageNumber(1)
            setPDF(null)
        }
        //переход на следующий элемент - не меняя позиций в образовательном курсе
        else {
            dfp(
                location.state.usereducationprofile_id,
                'Лекция по ' + location.state.edelement.shortname,
                'Лекция по ' + nextElement.shortname,
                'Кнопка перехода',
                'Движение вперёд',
                params,
            )
            navigate(`/lms/read/`, {
                state: {
                    edelement: nextElement,
                    course_id: location.state.course_id,
                    edelements: location.state.edelements,
                    usereducationprofile_id:
                        location.state
                            .usereducationprofile_id,
                    currentEdelement: location.state.currentEdelement,
                    plan_id: location.state.plan_id,
                },
            })
            setPrevElement(null)
            setNextElement(null)
            setPageNumber(1)
            setPDF(null)
        }
    }

    return (
        <Grid item xs={6}>
            <Button
                fullWidth
                variant='contained'
                sx={{m: 2, minWidth: 120}}
                onClick={handleClickNext}
            >
                {title}
            </Button>
        </Grid>
    )
}

function PrevElement({
    prevElement,
    location,
    setPrevElement,
    setNextElement,
    setPageNumber,
    setPDF,
    navigate,
}) {

    const movePreviousElement = () => {
        const params = '{usereducationcourse_id : ' +
                    location.state.usereducationprofile_id +
                    ', edelement_id: ' +
                    location.state.edelement.id +
                    '}'

        dfp(
            location.state.usereducationprofile_id,
            'Лекция по ' + location.state.edelement.shortname,
            'Лекция по ' + prevElement.shortname,
            'Кнопка перехода',
            'Движение назад',
            params,
        )
        navigate(`/lms/read/`, {
            state: {
                edelement: prevElement,
                course_id: location.state.course_id,
                edelements: location.state.edelements,
                usereducationprofile_id: location.state.usereducationprofile_id,
                currentEdelement: location.state.currentEdelement,
            },
        })
        setPrevElement(null)
        setNextElement(null)
        setPageNumber(1)
        setPDF(null)
    }

    return (
        <Grid item xs={6}>
            {prevElement != null &&
                <Button
                    fullWidth
                    variant='contained'
                    sx={{m: 2, minWidth: 120}}
                    onClick={movePreviousElement}
                >
                    Прошлый материал - {prevElement.name}
                </Button>
            }
        </Grid>
    )
}

function GetBack({location, navigate}) {

    const moveCourse = () => {
        const params = '{usereducationcourse_id : ' +
                    location.state.usereducationprofile_id +
                    ', edelement_id: ' +
                    location.state.edelement.id +
                    '}'

        dfp(
            location.state.usereducationprofile_id,
            'Лекция по ' + location.state.edelement.shortname,
            'Страница курса',
            'Кнопка выхода',
            'возвращение в оглавление',
            params,
        )
        navigate(`/lms/course`, {
            state: {
                course_id: location.state.course_id,
                usereducationprofile_id: location.state.usereducationprofile_id,
            },
        })
    }

    return (
        <Grid item xs={1}>
            <Tooltip title='Вернуться в курс'>
                <IconButton
                    aria-label='settings'
                    onClick={moveCourse}
                >
                    <ArrowUpwardIcon sx={{fontSize: 20}} color='success' />
                </IconButton>
            </Tooltip>
        </Grid>
    )
}
