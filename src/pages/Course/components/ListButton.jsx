import * as React from 'react'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import {ListItemIcon, Tooltip} from '@mui/material'
import Collapse from '@mui/material/Collapse'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import {useLocation, useNavigate} from 'react-router-dom'
import axios from '../../../utils/axios'
import DoneIcon from '@mui/icons-material/Done'
import PendingIcon from '@mui/icons-material/Pending'
import { dfp } from '../../../utils/dfp'

export default function ({subListItems, name, all, current}) {
    const navigate = useNavigate()
    const location = useLocation()
    const [open, setOpen] = React.useState(false)
    const handleClick = () => {
        setOpen(!open)
    }

    const moveElement = async (element) => {
        let edelementLink
        if (current.inUED == null) {
            edelementLink = current.object
            await axios
                .put(
                    `/api/user/course/${location.state.course_id}?current_edelement_id=${current.object.id}&score=1`,
                    {}
                )
                .then(({data}) => {
                    console.log(data)
                })
        } 
        else edelementLink = element
        dfp(
            location.state.usereducationprofile_id,
            'Курс по дисциплине',
            'Лекция по ' + edelementLink.shortname,
            current.inUED == null ? 'Начало курса' : 'Ссылка на лекцию',
            'Нажатие',
            "Учащийся перешёл на лекцию по" + edelementLink.shortname + ", edelement_id: " + element.id,
        )
        navigate(`/lms/read/`, {
            state: {
                edelement: edelementLink,
                course_id: location.state.course_id,
                edelements: all,
                usereducationprofile_id:
                    location.state.usereducationprofile_id,
                currentEdelement: current.object,
                plan_id: location.state.plan_id,
            },
        })
    }

    let listButtons = []

    subListItems.forEach((element) => {
        if (element.children_count == 0) {
            const currentPacknumber = current.object != null ? current.object.didacticdescription.packnumber : false
            const elementPacknumber = element.didacticdescription.packnumber

            let disabled = false
            if (currentPacknumber && (currentPacknumber < elementPacknumber))
                disabled = true
            
            listButtons.push(
                <ListItemButton
                    sx={{pl: 4}}
                    disabled={disabled}
                    onClick={() => {moveElement(element)}}
                >
                    <ListItemText primary={element.name} />
                    {currentPacknumber && (currentPacknumber > elementPacknumber) && 
                        <Tooltip title='Прочитано'>
                            <ListItemIcon>
                                <DoneIcon
                                    sx={{fontSize: 40}}
                                    color='success'
                                />
                            </ListItemIcon>
                        </Tooltip>
                    }
                    {currentPacknumber && (currentPacknumber == elementPacknumber) && 
                        <Tooltip title='Продолжить...'>
                            <ListItemIcon>
                                <PendingIcon
                                    sx={{fontSize: 20}}
                                    color='yellow'
                                />
                            </ListItemIcon>
                        </Tooltip>
                    }
                </ListItemButton>
            )
        }
    })

    return (
        <>
            <ListItemButton onClick={handleClick}>
                <ListItemText primary={name} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open} timeout='auto' unmountOnExit>
                <List component='div' disablePadding>
                    {listButtons}
                </List>
            </Collapse>
        </>
    )
}
