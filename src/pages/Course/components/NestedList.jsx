import * as React from 'react'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import ListButton from './ListButton'

export default function ({ listItems, all, current }) {
    let listButtons = []
    listItems.forEach((element) => {
        if (element.children_count > 0) {
            listButtons.push(
                <ListButton
                    subListItems={element.children}
                    name={element.name}
                    all={all}
                    current={current}
                />
            )
        } else {
            listButtons.push(
                <ListItemButton>
                    <ListItemText primary={element.name} />
                </ListItemButton>
            )
        }
    })
    return <>{listButtons}</>
}
