import { useState, useEffect } from 'react'
import axios from '../../../utils/axios'
import { Typography, Paper, Grid, Button } from '@mui/material'
import EnhancedSelect from '../../../components/EnhancedSelect'
import Tasks from './components/Tasks'
import Competences from './components/Competences'
import Edelements from './components/Edelements'
export default function () {
    const [tests, setTests] = useState([])
    const [quests, setQuests] = useState([])
    const [answers, setAnswers] = useState([])
    const [currentTest, setCurrentTest] = useState('')
    const [currentQuest, setCurrentQuest] = useState('')
    const [currentAnswer, setCurrentAnswer] = useState('')
    const [show, setShow] = useState(false)

    const [task, setTask] = useState(false)
    const [competence, setCompetence] = useState(false)
    const [edelement, setEdelement] = useState(false)

    const [list, setList] = useState([])

    useEffect(() => {
        axios.post(`/api/tests`, {}).then(({ data }) => {
            setTests(data.filter((elem) => elem.option === 0))
        })
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])

    useEffect(() => {
        if (currentTest !== '') {
            setCurrentQuest('')
            axios
                .get(`/api/test/${currentTest}/quests`, {})
                .then(({ data }) => {
                    setQuests(data)
                })
        }
    }, [currentTest])

    useEffect(() => {
        if (currentQuest !== '') {
            axios
                .get(`/api/quest/${currentQuest}/answers`, {})
                .then(({ data }) => {
                    console.log(data)
                    setAnswers(data)
                })
        }
    }, [currentQuest])

    if (show)
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Typography variant='h5' sx={{ textAlign: 'center', m: 2 }}>
                        Личные предпочтения. Связь вопросов анкеты.
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <EnhancedSelect
                        name={'Тесты'}
                        menuItems={tests}
                        setParentValue={setCurrentTest}
                        defaultValue={currentTest}
                    />
                </Grid>
                <Grid item xs={12}>
                    <EnhancedSelect
                        name={'Вопросы'}
                        menuItems={quests}
                        setParentValue={setCurrentQuest}
                        defaultValue={currentQuest}
                        quest
                    />
                </Grid>
                <Grid item xs={12}>
                    <EnhancedSelect
                        name={'Ответы'}
                        menuItems={answers}
                        setParentValue={setCurrentAnswer}
                        defaultValue={currentAnswer}
                        position
                    />
                </Grid>
                {currentAnswer !== '' && (
                    <>
                        <Grid item xs={4}>
                            <Button
                                variant='contained'
                                sx={{ mb: 2 }}
                                disabled={task || competence || edelement}
                                onClick={async () => {
                                    await axios
                                        .post(`/api/personals/`, {
                                            connectionType: 3,
                                            answer_id: currentAnswer,
                                        })
                                        .then(({ data }) => {
                                            setList(data)
                                        })
                                    setTask(true)
                                }}
                                fullWidth>
                                Задачи
                            </Button>
                        </Grid>
                        <Grid item xs={4}>
                            <Button
                                variant='contained'
                                sx={{ mb: 2 }}
                                disabled={task || competence || edelement}
                                onClick={async () => {
                                    await axios
                                        .post(`/api/personals/`, {
                                            connectionType: 2,
                                            answer_id: currentAnswer,
                                        })
                                        .then(({ data }) => {
                                            setList(data)
                                        })
                                    setCompetence(true)
                                }}
                                fullWidth>
                                Компетенции
                            </Button>
                        </Grid>
                        <Grid item xs={4}>
                            <Button
                                variant='contained'
                                sx={{ mb: 2 }}
                                disabled={task || competence || edelement}
                                onClick={async () => {
                                    await axios
                                        .post(`/api/personals/`, {
                                            connectionType: 1,
                                            answer_id: currentAnswer,
                                        })
                                        .then(({ data }) => {
                                            setList(data)
                                        })
                                    setEdelement(true)
                                }}
                                fullWidth>
                                Образовательные элементы
                            </Button>
                        </Grid>
                    </>
                )}
                <Grid item xs={12}>
                    <Tasks
                        open={task}
                        setOpen={setTask}
                        list={list}
                        answer={currentAnswer}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Competences
                        open={competence}
                        setOpen={setCompetence}
                        list={list}
                        answer={currentAnswer}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Edelements
                        open={edelement}
                        setOpen={setEdelement}
                        list={list}
                        answer={currentAnswer}
                    />
                </Grid>
            </Grid>
        )
}
