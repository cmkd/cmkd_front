import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import { Dialog, DialogContent, DialogTitle, Grid } from '@mui/material'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
export default function ({ open, setOpen, list, answer, reload, setReload }) {
    const handleClose = () => {
        setOpen(false)
    }

    const [tasks, setTasks] = useState()

    useEffect(() => {
        if (open)
            axios
                .post(`/api/tasks`, {
                    perPage: 1000,
                })
                .then(({ data }) => {
                    let m = []
                    data.items.map(task => {
                        let check = false

                        if (list.find(elem => elem.task_id == task.id)) {
                            check = true
                        }

                        m.push({
                            id: task.id,
                            name: task.name,
                            checked: check,
                        })
                    })
                    setTasks(m)
                })
    }, [open])

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                Задачи
            </DialogTitle>
            <DialogContent>
                <Grid container>
                    <Grid item xs={12}>
                        {tasks !== undefined ? (
                            <CheckboxList
                                list={tasks}
                                answer_id={answer}
                                reload={reload}
                                setReload={setReload}
                            />
                        ) : null}
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
}

function CheckboxList({ list, answer_id, reload, setReload }) {
    const [state, setState] = useState(list)
    const [checked, setChecked] = useState({
        id: -1,
        checked: false,
    })

    useEffect(() => {
        if (checked.id !== -1) {
            /**
             * добавление связи
             */
            if (checked.checked === true) {
                axios
                    .post(`/api/personal/`, {
                        answer_id: answer_id,
                        task_id: checked.id,
                        connectionType: 3,
                    })
                    .then(({ data }) => {
                        console.log(data)
                    })
            }
            /**
             * удаление связи
             */
            if (checked.checked === false) {
                axios
                    .delete(
                        `/api/personal?answer_id=${answer_id}&task_id=${checked.id}`,
                        {},
                    )
                    .then(({ data }) => {
                        console.log(data)
                    })
            }
            //setReload(!reload)
        }
    }, [checked])

    const handleChange = event => {
        let newArr = []
        state.map(item => {
            newArr.push(item)
            if (event.target.name === String(item.id)) {
                newArr[newArr.length - 1].checked = event.target.checked
                setChecked({ id: item.id, checked: item.checked })
            }
        })
        setState(newArr)
    }

    return (
        <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
            {state.map(item => {
                const labelId = `checkbox-list-label-${item.id}`

                return (
                    <ListItem
                        key={item.id}
                        disablePadding
                        secondaryAction={
                            <Checkbox
                                name={String(item.id)}
                                edge="start"
                                checked={item.checked}
                                onChange={handleChange}
                                inputProps={{ 'aria-labelledby': labelId }}
                            />
                        }
                    >
                        <ListItemText id={labelId} primary={item.name} />
                    </ListItem>
                )
            })}
        </List>
    )
}
