import Table from '../../../components/Table'
import * as React from 'react'
import axios from '../../../utils/axios'
export default function () {
    const columnList = [
        {
            name: 'ФИО',
            type: 'fullname',
            dataType: 'user_id',
        },
        {
            name: 'Учебный план',
            type: 'plan',
            dataType: 'edelement_id',
        },
        {
            name: 'Начало обучения',
            type: 'started_at',
            dataType: 'date',
        },
        {
            name: 'Конец обучения',
            type: 'finished_at',
            dataType: 'date',
        },
    ]

    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])
    if (show)
        return (
            <>
                <Table
                    type={'user/profiles'}
                    oneItemType={'user/profile'}
                    columnList={columnList}
                    edelementLevel={1}
                />
            </>
        )
}
