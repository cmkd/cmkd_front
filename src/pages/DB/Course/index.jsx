import Table from '../../../components/Table'
import * as React from 'react'
import axios from '../../../utils/axios'
export default function () {
    const columnList = [
        {
            name: 'Наименование',
            type: 'name',
            dataType: 'string',
        },
        {
            name: 'О курсе',
            type: 'about',
            dataType: 'string',
        },
        {
            name: 'Дисциплина',
            type: 'plan',
            dataType: 'edelement_id',
        },
    ]
    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])
    if (show)
        return (
            <>
                <Table
                    type={'courses'}
                    oneItemType={'course'}
                    columnList={columnList}
                    edelementLevel={3}
                />
            </>
        )
}
