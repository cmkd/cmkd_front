import { Button, Grid, Paper } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import React from 'react'
import axios from '../../utils/axios'

export default function () {
    const navigate = useNavigate()

    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])
    if (show)
        return (
            <Paper>
                <Grid container>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/edelement')}>
                            Таблица образовательных элементов
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/competence')}>
                            Таблица компетенций
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/personal')}>
                            Таблица личных предпочтений
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/method')}>
                            Таблица методов
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/task')}>
                            Таблица задач
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/files')}>
                            Файлы
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/profiles')}>
                            Образовательные профили
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/courses')}>
                            Образовательные курсы
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ m: 2, minWidth: 120 }}
                            onClick={() => navigate('/lms/db/dfp')}>
                            Скачать ОЦС
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        )
}
