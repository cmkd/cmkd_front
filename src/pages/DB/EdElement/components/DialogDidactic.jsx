import Grid from '@mui/material/Grid'
import { Dialog, DialogContent, DialogTitle } from '@mui/material'
import Connection from './Connection'
import Didactic from './Didactic'
export default function ({
    open,
    setOpen,
    didactic,
    setDidactic,
    didacticFields,
    didacticForeignList,
    disabled,
    elem,
    reload,
    setReload,
    discipline,
}) {
    const handleClose = () => {
        setOpen(false)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                характеристики ДЕ
            </DialogTitle>
            <DialogContent>
                <Grid item xs={12}>
                    <Didactic
                        didactic={didactic}
                        setDidactic={setDidactic}
                        didacticFields={didacticFields}
                        didacticForeignList={didacticForeignList}
                        disabled={disabled}
                    />
                    <Connection
                        elem={elem}
                        reload={reload}
                        setReload={setReload}
                        discipline={discipline}
                    />
                </Grid>
            </DialogContent>
        </Dialog>
    )
}
