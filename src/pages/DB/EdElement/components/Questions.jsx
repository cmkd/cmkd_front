import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import { Dialog, DialogContent, DialogTitle, Grid, Button } from '@mui/material'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import ListItemButton from '@mui/material/ListItemButton'
import EnhancedSelect from '../../../../components/EnhancedSelect'

export default function ({ open, setOpen, elem, reload, setReload }) {
    const handleClose = () => {
        setOpen(false)
    }

    const [add, setAdd] = useState(false)
    const [del, setDel] = useState(false)

    const [tests, setTests] = useState([])
    const [quests, setQuests] = useState([])
    const [currentTest, setCurrentTest] = useState('')
    const [currentQuest, setCurrentQuest] = useState('')
    const [currentConnect, setCurrentConnect] = useState(null)

    let bodyRows
    bodyRows = elem.quests.map(elem => (
        <ListItem disablePadding>
            <ListItemButton
                onClick={() => {
                    setCurrentConnect(elem.id)
                    setAdd(false)
                    setDel(true)
                }}
            >
                <ListItemText primary={elem.pivot.position + ' ' + elem.name} />
            </ListItemButton>
        </ListItem>
    ))
    useEffect(() => {
        axios.post(`/api/tests`, {}).then(({ data }) => {
            setTests(data)
        })
    }, [])

    useEffect(() => {
        if (currentTest !== '') {
            setCurrentQuest('')
            axios
                .get(`/api/test/${currentTest}/quests`, {})
                .then(({ data }) => {
                    setQuests(data)
                })
        }
    }, [currentTest])
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                Связь с вопросами {elem.name}
            </DialogTitle>
            <DialogContent>
                <Grid container>
                    <Grid item xs={12}>
                        <List
                            sx={{
                                width: '100%',
                                maxWidth: 1000,
                                bgcolor: 'background.paper',
                                position: 'relative',
                                overflow: 'auto',
                                maxHeight: 600,
                                '& ul': { padding: 0 },
                            }}
                        >
                            {bodyRows}
                        </List>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant="contained"
                            sx={{ mb: 2, maxWidth: 300 }}
                            onClick={() => {
                                setAdd(true)
                                setDel(false)
                            }}
                            fullWidth
                        >
                            Добавить
                        </Button>
                    </Grid>
                    {del ? (
                        <Grid item xs={4}>
                            <Button
                                variant="contained"
                                sx={{ mb: 2, ml: 2, maxWidth: 300 }}
                                onClick={() => {
                                    axios
                                        .delete(
                                            `/api/edelementlinkquest?edelement_id=${elem.id}&quest_id=${currentConnect}`,
                                            {},
                                        )
                                        .then(({ data }) => {
                                            console.log(data)
                                        })
                                    setDel(false)
                                    setReload(!reload)
                                }}
                                fullWidth
                            >
                                Удалить
                            </Button>
                        </Grid>
                    ) : null}
                    {add || del ? (
                        <Grid item xs={4}>
                            <Button
                                variant="contained"
                                sx={{ mb: 2, ml: 2, maxWidth: 300 }}
                                onClick={() => {
                                    setAdd(false)
                                    setDel(false)
                                    setCurrentTest('')
                                    setCurrentQuest('')
                                }}
                                fullWidth
                            >
                                Отменить
                            </Button>
                        </Grid>
                    ) : null}
                    {add ? (
                        <>
                            <Grid item xs={12}>
                                <EnhancedSelect
                                    name={'Тесты'}
                                    menuItems={tests}
                                    setParentValue={setCurrentTest}
                                    defaultValue={currentTest}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <EnhancedSelect
                                    name={'Вопросы'}
                                    menuItems={quests}
                                    setParentValue={setCurrentQuest}
                                    defaultValue={currentQuest}
                                    quest
                                />
                            </Grid>
                        </>
                    ) : null}
                    {currentQuest ? (
                        <Button
                            variant="contained"
                            sx={{ mb: 2, maxWidth: 300 }}
                            onClick={async () => {
                                await axios
                                    .post(`/api/edelementlinkquest`, {
                                        edelement_id: elem.id,
                                        quest_id: currentQuest,
                                        position: quests.find(
                                            ({ id }) => id == currentQuest,
                                        ).pivot.position,
                                    })
                                    .then(({ data }) => {
                                        console.log(data)
                                    })
                                setAdd(false)
                                setCurrentQuest('')
                                setCurrentTest('')
                                setReload(!reload)
                            }}
                            fullWidth
                        >
                            Сохранить
                        </Button>
                    ) : null}
                </Grid>
            </DialogContent>
        </Dialog>
    )
}
