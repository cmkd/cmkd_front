import React from 'react'
import TextField from '@mui/material/TextField'
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import EnhancedSelect from '../../../../components/EnhancedSelect'
import axios from '../../../../utils/axios'
export default function ({
    didactic,
    setDidactic,
    didacticFields,
    didacticForeignList,
    disabled,
}) {
    const [files, setFiles] = React.useState([])

    const handleChange = (event) => {
        setDidactic((prev) => ({
            ...prev,
            ['test_type']: event.target.value,
        }))
    };
    
    React.useEffect(() => {
        async function fileListLoading() {
            await axios
                .post(`/api/files`, {
                    pageCount: 1,
                    perPage: 1000,
                })
                .then(({ data }) => {
                    let f = []
                    f.push({
                        id: null,
                        name: 'нет',
                    })
                    f = f.concat(data.items)
                    setFiles(f)
                })
        }
        fileListLoading()
    }, [])

    let didacticTextFields = []
    didacticFields.map((field) => {
        if (field.input === 'TextField') {
            didacticTextFields.push(
                <TextField
                    key={field.name}
                    fullWidth
                    label={field.name}
                    value={didactic[field.type]}
                    onChange={(event) => {
                        setDidactic((prev) => ({
                            ...prev,
                            [field.type]: event.target.value,
                        }))
                    }}
                    sx={{ mb: 2 }}
                    disabled={disabled}
                />
            )
        }
        if (field.input === 'Select') {
            didacticTextFields.push(
                <EnhancedSelect
                    key={field.name}
                    name={field.name}
                    type={field.type}
                    menuItems={didacticForeignList[field.type]}
                    setParentValue={setDidactic}
                    defaultValue={didactic[field.type]}
                    disabled={disabled}
                />
            )
        }
    })
    didacticTextFields.push(
        <EnhancedSelect
            key={'Файлы'}
            name={'Файлы'}
            type={'filestorage_id'}
            menuItems={files}
            setParentValue={setDidactic}
            defaultValue={didactic['filestorage_id']}
            disabled={disabled}
        />
    )
    return <>
        {didacticTextFields}
        <FormControl>
            <FormLabel id="demo-row-radio-buttons-group-label">LMS - Поведение после изучения ДЕ</FormLabel>
            <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="row-radio-buttons-group"
                value={didactic['test_type']}
                onChange={handleChange}
                
            >
                <FormControlLabel value="u" control={<Radio />} label="Тест по ДЕ" disabled={disabled}/>
                <FormControlLabel value="t" control={<Radio />} label="Тест по теме" disabled={disabled}/>
                <FormControlLabel value="d" control={<Radio />} label="Тест по дисциплине" disabled={disabled}/>
            </RadioGroup>
        </FormControl>
    </>
}
