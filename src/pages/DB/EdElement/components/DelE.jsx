import { useState } from 'react'
import axios from '../../../../utils/axios'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import { Dialog, DialogContent, Typography } from '@mui/material'

export default function ({
    name,
    id,
    reload,
    setReload,
    del,
    setDel,
    oneItemType,
}) {
    const [open, setOpen] = useState(del)
    const handleClose = () => {
        setOpen(false)
        setDel(!del)
    }
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogContent>
                <Grid item xs={12}>
                    <Typography>Подтвердите удаление {name}</Typography>
                </Grid>
                <Button
                    variant="contained"
                    sx={{ margin: 2 }}
                    onClick={event => {
                        setDel(!del)
                        axios
                            .delete(`/api/${oneItemType}/${id}`, {})
                            .then(({ data }) => {
                                console.log(data)
                            })
                        setReload(!reload)
                        setOpen(false)
                    }}
                >
                    Удалить
                </Button>
                <Button
                    variant="contained"
                    sx={{ margin: 2 }}
                    onClick={event => {
                        setDel(!del)
                        setOpen(false)
                    }}
                >
                    Отменить
                </Button>
            </DialogContent>
        </Dialog>
    )
}
