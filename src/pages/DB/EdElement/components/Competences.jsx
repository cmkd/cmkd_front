import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import SkeletonTable from '../../../../components/Skeleton/Table'
import Table from '@mui/material/Table'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TablePagination from '@mui/material/TablePagination'
import Button from '@mui/material/Button'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import EditIcon from '@mui/icons-material/Edit'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import { Delete } from '@mui/icons-material'
import TextField from '@mui/material/TextField'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import { Dialog, DialogContent, DialogTitle, Typography } from '@mui/material'
import { pink } from '@mui/material/colors'
import EnhancedSelect from '../../../../components/EnhancedSelect'

const pathList = [
    {
        name: 'placeholder',
        type: '0',
        num: 0,
    },
    {
        name: 'Группа компетенций',
        type: '1',
        num: 1,
    },
    {
        name: 'Подгруппа компетенций',
        type: '2',
        num: 2,
    },
    {
        name: 'Частицы компетентностного профиля',
        type: '3',
        num: 3,
    },
]

export default function ({ open, setOpen, elem, reload, setReload }) {
    const [state, setState] = useState({
        page: 0,
        perPage: 5,
        items: [],
        total: 0,
        loading: true,
        completed: false,
    })

    const [add, setAdd] = useState(false)
    const [del, setDel] = useState(false)
    const [currentItem, setCurrentItem] = useState(0)

    const [newConnection, setNewConnection] = useState({
        0: '',
        1: '',
        2: '',
        3: '',
    })
    const [editedType, setEditedType] = useState('')
    const [newConnectionList, setNewConnectionList] = useState({
        0: [],
        1: [],
        2: [],
        3: [],
    })

    const [bodyRows, setBodyRows] = useState('')

    useEffect(() => {
        //для создания нового соединения - загрузка нулевого уровня
        if (add === true)
            axios
                .post(`/api/competences`, {
                    parent_id: null,
                })
                .then(({ data }) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order,
                        )
                        setNewConnectionList(prev => ({
                            ...prev,
                            0: sortedData,
                        }))
                    }
                })
    }, [add])

    useEffect(() => {
        //для создания нового соединения - загрузка остальных уровней
        if (editedType !== '3') {
            let type
            let current = pathList.filter(obj => {
                return obj.type === editedType
            })
            if (current[0] !== undefined) {
                let next = pathList.filter(obj => {
                    return obj.num === current[0].num + 1
                })
                type = next[0].type
            }

            axios
                .post(`/api/competences`, {
                    parent_id: newConnection[editedType],
                })
                .then(({ data }) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order,
                        )
                        setNewConnectionList(prev => ({
                            ...prev,
                            [type]: sortedData,
                        }))
                    }
                })
        }
    }, [editedType, newConnection])

    useEffect(() => {
        setState(prev => ({
            ...prev,
            items: elem.competences,
            loading: false,
            total: elem.competences.length,
        }))
        console.log(elem.competences)
        if (elem.competences) {
            setBodyRows(elem.competences
                .slice(
                    state.page * state.perPage,
                    state.page * state.perPage + state.perPage,
                )
                .map(elem => (
                    <TableRow
                        key={'pt-' + elem.id}
                        //onClick={}
                        sx={{ '&:hover': { background: '#f1f1f1' } }}
                    >
                        {/**----------------------------------------------------------------------*/}
                        <>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.name}
                            </TableCell>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.code}
                            </TableCell>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.shortname}
                            </TableCell>
                        </>
                        {/**----------------------------------------------------------------------*/}
                        <TableCell sx={{ fontSize: 'h6.fontSize' }} width={'2%'}>
                            <Tooltip title="Удалить">
                                <IconButton
                                    aria-label="settings"
                                    onClick={event => {
                                        setDel(true)
                                        setCurrentItem(elem.id)
                                    }}
                                    disabled={add || del}
                                >
                                    <Delete
                                        sx={{ color: pink[500], fontSize: 20 }}
                                    />
                                </IconButton>
                            </Tooltip>
                        </TableCell>
                    </TableRow>
                )))
        }
    }, [elem])

    const handleClose = () => {
        setOpen(false)
    }

    const handleChangePage = (_, newPage) => {
        if (newPage < 0 || newPage * state.perPage >= state.total) return

        setState(prev => ({ ...prev, page: newPage }))
    }

    const handleChangeRowsPerPage = event => {
        const newPerPage = parseInt(event.target.value)

        setState(prev => ({
            ...prev,
            perPage: newPerPage,
        }))
    }

    let addConnectionSelects = []
    pathList.map(step => {
        addConnectionSelects.push(
            <EnhancedSelect
                name={step.name}
                type={step.type}
                mode={'competences'}
                menuItems={newConnectionList[step.type]}
                setParentValue={setNewConnection}
                defaultValue={newConnection[step.type]}
                setType={setEditedType}
            />,
        )
    })

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                Связь с компетенциями {elem.name}
            </DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 400 }}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell
                                            sx={{ fontSize: 'h6.fontSize' }}
                                            width="auto"
                                        >
                                            Развивает
                                        </TableCell>
                                        <TableCell
                                            sx={{ fontSize: 'h6.fontSize' }}
                                            width="20%"
                                        >
                                            Шифр
                                        </TableCell>
                                        <TableCell
                                            sx={{ fontSize: 'h6.fontSize' }}
                                            width="auto"
                                        >
                                            Кратко
                                        </TableCell>
                                        <TableCell>
                                            <Tooltip title="Новая связь">
                                                <IconButton
                                                    aria-label="settings"
                                                    onClick={event => {
                                                        event.stopPropagation()
                                                        setAdd(true)
                                                    }}
                                                    disabled={add || del}
                                                >
                                                    <AddIcon
                                                        color="success"
                                                        sx={{ fontSize: 20 }}
                                                    />
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>{bodyRows}</TableBody>
                            </Table>
                        </TableContainer>

                        <TablePagination
                            rowsPerPageOptions={[5, 10, 15, 25]}
                            component="div"
                            count={state.total}
                            page={state.page}
                            rowsPerPage={state.perPage}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            labelRowsPerPage="Строк:"
                            labelDisplayedRows={({ from, to, count }) =>
                                `${from} - ${to} из ${count}`
                            }
                        />
                    </Grid>
                    {add ? (
                        <>
                            <Grid item xs={12}>
                                {addConnectionSelects}
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    fullWidth
                                    variant="contained"
                                    sx={{ width: '100%' }}
                                    onClick={event => {
                                        event.stopPropagation()
                                        setAdd(!add)
                                        setNewConnection({
                                            1: '',
                                            2: '',
                                            3: '',
                                        })
                                        setNewConnectionList({
                                            1: [],
                                            2: [],
                                            3: [],
                                        })
                                    }}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            {newConnection.de !== 0 ? (
                                <Grid item xs={6}>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        sx={{ width: '100%' }}
                                        onClick={async event => {
                                            event.stopPropagation()
                                            setAdd(!add)
                                            let external = null

                                            await axios
                                                .post(
                                                    `/api/edelementlinkcomp`,
                                                    {
                                                        edelement_id: elem.id,
                                                        competence_id:
                                                            newConnection[3],
                                                    },
                                                )
                                                .then(({ data }) => {
                                                    console.log(data)
                                                })

                                            setReload(!reload)
                                        }}
                                    >
                                        Сохранить
                                    </Button>
                                </Grid>
                            ) : null}
                        </>
                    ) : null}
                    {del ? (
                        <Grid item xs={6}>
                            <Button
                                fullWidth
                                variant="contained"
                                sx={{ width: '100%' }}
                                onClick={async event => {
                                    event.stopPropagation()

                                    await axios
                                        .delete(
                                            `/api/edelementlinkcomp?edelement_id=${elem.id}&competence_id=${currentItem}`,
                                            {},
                                        )
                                        .then(({ data }) => {
                                            console.log(data)
                                        })
                                    setDel(!del)
                                    setReload(!reload)
                                }}
                            >
                                Удалить
                            </Button>
                        </Grid>
                    ) : null}
                </Grid>
            </DialogContent>
        </Dialog>
    )
}
