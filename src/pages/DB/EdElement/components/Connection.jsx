import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import SkeletonTable from '../../../../components/Skeleton/Table'
import Table from '@mui/material/Table'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TablePagination from '@mui/material/TablePagination'
import Button from '@mui/material/Button'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import EditIcon from '@mui/icons-material/Edit'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import { Delete } from '@mui/icons-material'
import TextField from '@mui/material/TextField'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import { Dialog, DialogContent, DialogTitle, Typography } from '@mui/material'
import { pink } from '@mui/material/colors'
import EnhancedSelect from '../../../../components/EnhancedSelect'

const pathList = [
    {
        name: 'Учебный план',
        type: 'plan',
        num: 1,
    },
    {
        name: 'Семестр',
        type: 'sem',
        num: 2,
    },
    {
        name: 'Дисциплина',
        type: 'disc',
        num: 3,
    },
    {
        name: 'Тема',
        type: 'theme',
        num: 4,
    },
    {
        name: 'Дидактическая единица',
        type: 'de',
        num: 5,
    },
]

export default function ({ elem, reload, setReload, discipline, multilevel }) {
    const [state, setState] = useState({
        page: 0,
        perPage: 5,
        items: [],
        total: 0,
        loading: true,
        completed: false,
    })

    const [add, setAdd] = useState(false)
    const [del, setDel] = useState(false)
    const [edit, setEdit] = useState(false)
    const [currentItem, setCurrentItem] = useState(0)
    const [about, setAbout] = useState('')

    const [newConnection, setNewConnection] = useState({
        plan: '',
        sem: '',
        disc: '',
        theme: '',
        de: '',
        dePosition: 0,
    })
    const [editedType, setEditedType] = useState('')
    const [newConnectionList, setNewConnectionList] = useState({
        plan: [],
        sem: [],
        disc: [],
        theme: [],
        de: [],
    })

    const [bodyRows, setBodyRows] = useState()

    useEffect(() => {
        //для создания нового соединения - загрузка учебных планов
        if (add === true)
            axios
                .post(`/api/edelements`, {
                    parent_id: null,
                })
                .then(({ data }) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order
                        )
                        setNewConnectionList((prev) => ({
                            ...prev,
                            plan: sortedData,
                        }))
                    }
                })
    }, [add])

    useEffect(() => {
        //для создания нового соединения - загрузка остальных образ. элементов
        if (editedType !== 'de') {
            let type
            let current = pathList.filter((obj) => {
                return obj.type === editedType
            })
            if (current[0] !== undefined) {
                let next = pathList.filter((obj) => {
                    return obj.num === current[0].num + 1
                })
                type = next[0].type
            }

            axios
                .post(`/api/edelements`, {
                    parent_id: newConnection[editedType],
                })
                .then(({ data }) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order
                        )
                        setNewConnectionList((prev) => ({
                            ...prev,
                            [type]: sortedData,
                        }))
                    }
                })
        }
    }, [editedType, newConnection])

    useEffect(() => {
        //для отображения root путей у соединений
        let items = []

        elem.connections.forEach((element) => {
            let rootPath = element.short_parent.name + '/'
            let parent = element.short_parent
            while (parent.short_parent != null) {
                rootPath += parent.short_parent.name + '/'
                parent = parent.short_parent
            }
            items.push({
                id: element.connection_id,
                name: element.name,
                rootPath: rootPath,
                about: element.about,
                position: element.position,
            })
        })
        
        setState((prev) => ({
            ...prev,
            items: items,
            loading: false,
            total: items.length,
        }))
        if (items)
        setBodyRows(
            items
                .slice(
                    state.page * state.perPage,
                    state.page * state.perPage + state.perPage
                )
                .map((elem) => (
                    <TableRow
                        key={'pt-' + elem.id}
                        //onClick={}
                        sx={{ '&:hover': { background: '#f1f1f1' } }}>
                        {/**----------------------------------------------------------------------*/}
                        <>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.position + ' ' + elem.name}
                            </TableCell>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.rootPath}
                            </TableCell>
                            <TableCell sx={{ fontSize: 'h6.fontSize' }}>
                                {elem.about}
                            </TableCell>
                        </>
                        {/**----------------------------------------------------------------------*/}
                        <TableCell sx={{ fontSize: 'h6.fontSize' }} width={'2%'}>
                            <Tooltip title='Отредактировать'>
                                <IconButton
                                    aria-label='settings'
                                    onClick={(event) => {
                                        setEdit(true)
                                        setCurrentItem(elem.id)
                                    }}
                                    disabled={add || del || edit}>
                                    <EditIcon sx={{ fontSize: 20 }} />
                                </IconButton>
                            </Tooltip>
                        </TableCell>
                        <TableCell sx={{ fontSize: 'h6.fontSize' }} width={'2%'}>
                            <Tooltip title='Удалить'>
                                <IconButton
                                    aria-label='settings'
                                    onClick={(event) => {
                                        setDel(true)
                                        setCurrentItem(elem.id)
                                    }}
                                    disabled={add || del || edit}>
                                    <Delete
                                        sx={{ color: pink[500], fontSize: 20 }}
                                    />
                                </IconButton>
                            </Tooltip>
                        </TableCell>
                    </TableRow>
                ))
        )
    }, [elem])

    const handleChangePage = (_, newPage) => {
        if (newPage < 0 || newPage * state.perPage >= state.total) return

        setState((prev) => ({ ...prev, page: newPage }))
    }

    const handleChangeRowsPerPage = (event) => {
        const newPerPage = parseInt(event.target.value)

        setState((prev) => ({
            ...prev,
            perPage: newPerPage,
        }))
    }

    let addConnectionSelects = []
    pathList.map((step) => {
        addConnectionSelects.push(
            <EnhancedSelect
                name={step.name}
                type={step.type}
                menuItems={newConnectionList[step.type]}
                setParentValue={setNewConnection}
                defaultValue={newConnection[step.type]}
                setType={setEditedType}
            />
        )
    })

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Typography sx={{ textAlign: 'center', m: 2 }}>
                    Связи с другими элементами
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 400 }}>
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    sx={{ fontSize: 'h6.fontSize' }}
                                    width='auto'>
                                    Опирается на
                                </TableCell>
                                <TableCell
                                    sx={{ fontSize: 'h6.fontSize' }}
                                    width='50%'>
                                    Расположение
                                </TableCell>
                                <TableCell
                                    sx={{ fontSize: 'h6.fontSize' }}
                                    width='auto'>
                                    Комментарий
                                </TableCell>
                                <TableCell>
                                    <Tooltip title='Новая связь'>
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                event.stopPropagation()
                                                setAdd(true)
                                            }}
                                            disabled={add || del || edit}>
                                            <AddIcon
                                                color='success'
                                                sx={{ fontSize: 20 }}
                                            />
                                        </IconButton>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>{bodyRows}</TableBody>
                    </Table>
                </TableContainer>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 15, 25]}
                    component='div'
                    count={state.total}
                    page={state.page}
                    rowsPerPage={state.perPage}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage='Строк:'
                    labelDisplayedRows={({ from, to, count }) =>
                        `${from} - ${to} из ${count}`
                    }
                />
            </Grid>
            {add ? (
                <>
                    <Grid item xs={12}>
                        {addConnectionSelects}
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ width: '100%' }}
                            onClick={(event) => {
                                event.stopPropagation()
                                setAdd(!add)
                                setNewConnection({
                                    plan: '',
                                    sem: '',
                                    disc: '',
                                    theme: '',
                                    de: '',
                                    dePosition: 0,
                                })
                                setNewConnectionList({
                                    plan: [],
                                    sem: [],
                                    disc: [],
                                    theme: [],
                                    de: [],
                                })
                            }}>
                            Отменить
                        </Button>
                    </Grid>
                    {newConnection.de !== 0 ? (
                        <Grid item xs={6}>
                            <Button
                                fullWidth
                                variant='contained'
                                sx={{ width: '100%' }}
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setAdd(!add)
                                    let external = null
                                    if (!multilevel)
                                        if (
                                            discipline.id !== newConnection.disc
                                        )
                                            external = 1
                                    axios
                                        .post(`/api/eeconnect`, {
                                            edelement_from_id: elem.id,
                                            edelement_to_id:
                                                newConnection[editedType],
                                            position: newConnection.dePosition,
                                            external: external,
                                        })
                                        .then(({ data }) => {
                                            console.log(data)
                                        })

                                    setReload(!reload)
                                }}>
                                Сохранить
                            </Button>
                        </Grid>
                    ) : null}
                </>
            ) : null}
            {edit ? (
                <>
                    <TextField
                        fullWidth
                        label={'Комментарий'}
                        value={about}
                        onChange={(event) => {
                            setAbout(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ width: '100%' }}
                            onClick={(event) => {
                                event.stopPropagation()
                                setEdit(!edit)
                            }}>
                            Отменить
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ width: '100%' }}
                            onClick={(event) => {
                                event.stopPropagation()
                                axios
                                    .put(`/api/eeconnect/${currentItem}`, {
                                        about: about,
                                    })
                                    .then(({ data }) => {
                                        console.log(data)
                                    })
                                setEdit(!edit)
                                setReload(!reload)
                            }}>
                            Сохранить
                        </Button>
                    </Grid>
                </>
            ) : null}
            {del ? (
                <Grid item xs={6}>
                    <Button
                        fullWidth
                        variant='contained'
                        sx={{ width: '100%' }}
                        onClick={(event) => {
                            event.stopPropagation()

                            axios
                                .delete(`/api/eeconnect/${currentItem}`, {})
                                .then(({ data }) => {
                                    console.log(data)
                                })
                            setDel(!del)
                            setReload(!reload)
                        }}>
                        Удалить
                    </Button>
                </Grid>
            ) : null}
        </Grid>
    )
}
