import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import { Dialog, DialogContent, DialogTitle } from '@mui/material'
import EnhancedSelect from '../../../../components/EnhancedSelect'
import Didactic from './Didactic'
import Connection from './Connection'
export default function ({
    add,
    setAdd,
    parent,
    elemType,
    setReload,
    reload,
    oneItemType,
    order,
    didacticForeignList,
    elem, //редактирование; undefined - добавление, {...} - редактирование
    didacticFieldsList,
}) {
    const [branches, setBranches] = useState([])
    const [disciplines, setDisciplines] = useState([])
    const [discipline_id, setDiscipline_id] = useState(
        elem !== undefined ? elem.discipline_id : ''
    )
    const [branch_id, setBranch_id] = useState(
        elem !== undefined ? elem.branch_id : ''
    )
    const [name, setName] = useState(elem !== undefined ? elem.name : '')
    const [shortname, setShortname] = useState(
        elem !== undefined ? elem.shortname : ''
    )
    const [control, setControl] = useState(
        elem !== undefined ? elem.control_id : ''
    )
    const [discform, setDiscform] = useState(
        elem !== undefined ? elem.discform_id : ''
    )

    const [open, setOpen] = useState(add)
    const [didactic, setDidactic] = useState(
        elem !== undefined && elemType.id === 5
            ? {
                  type_id: elem.didacticdescription.type_id,
                  core_id: elem.didacticdescription.core_id,
                  importance: elem.didacticdescription.importance,
                  difficulty: elem.didacticdescription.difficulty,
                  viewtype: elem.didacticdescription.viewtype,
                  packnumber: elem.didacticdescription.packnumber,
                  norms: elem.didacticdescription.norms,
                  profile: elem.didacticdescription.profile,
                  filestorage_id: elem.didacticdescription.filestorage_id,
                  test_type: elem.didacticdescription.test_type,
              }
            : {
                  type_id: '',
                  core_id: '',
                  importance: '',
                  difficulty: '',
                  viewtype: '',
                  packnumber: '',
                  norms: '',
                  profile: '',
                  filestorage_id: '',
                  test_type: '',
              }
    )
    const handleClose = () => {
        setOpen(false)
        setAdd(!add)
    }

    const handleSave = () => {
        setOpen(!open)
        setAdd(!add)

        if (elem !== undefined)
            axios
                .put(`/api/${oneItemType}/${elem.id}`, {
                    name: name,
                    shortname: shortname,
                    norms: didactic.norms,
                    profile: didactic.profile,
                    control_id: control,
                    discform_id: discform,
                    type_id: didactic.type_id,
                    importance: didactic.importance,
                    difficulty: didactic.difficulty,
                    //filepath: didactic.filepath,
                    viewtype: didactic.viewtype,
                    packnumber: didactic.packnumber,
                    core_id: didactic.core_id,
                    filestorage_id: didactic.filestorage_id,
                    edelementtype_id: elemType.id,
                    test_type: didactic.test_type,
                })
                .then(({ data }) => {
                    console.log(data)
                })
        else
            axios
                .post(`/api/${oneItemType}/create`, {
                    name: name,
                    shortname: shortname,
                    order: order,
                    level: elemType.id - 1,
                    parent_id:
                        parent.level != -1
                            ? parent.parentId
                            : null, //parent.parentId,
                    norms: didactic.norms,
                    profile: didactic.profile,
                    edelementtype_id: elemType.id,
                    control_id: control,
                    discform_id: discform,
                    type_id: didactic.type_id,
                    importance: didactic.importance,
                    difficulty: didactic.difficulty,
                    //filepath: didactic.filepath,
                    viewtype: didactic.viewtype,
                    packnumber: didactic.packnumber,
                    core_id: didactic.core_id,
                    filestorage_id: didactic.filestorage_id,
                    branch_id: branch_id,
                    discipline_id: discipline_id,
                    test_type: didactic.test_type,
                })
                .then(({ data }) => {
                    console.log(data)
                })

        setReload(!reload)
    }

    useEffect(() => {
        console.log(elem)
        async function branchload() {
            await axios.post(`/api/branches/short`, {}).then(({ data }) => {
                setBranches(data)
            })
        }
        async function disciplineload() {
            await axios.post(`/api/disciplines/short`, {}).then(({ data }) => {
                setDisciplines(data)
            })
        }

        if (parent.level > -1) {
            setDiscipline_id(parent.discipline_id)
            setBranch_id(parent.branch_id)
            
        }
        if (elemType.id == 1) {
            branchload()
        }
        if (elemType.id == 3) {
            disciplineload()
        }
    }, [])

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby='scroll-dialog-title'
            aria-describedby='scroll-dialog-description'>
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}>
                {elem !== undefined ? 'Редактировать' : 'Добавить'}{' '}
                {elemType.name}
            </DialogTitle>
            <DialogContent>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        label='Надэлемент'
                        value={parent.level != -1 ? parent.parentName : 'нет'}
                        sx={{ mb: 2 }}
                        disabled
                    />
                    <TextField
                        fullWidth
                        label='Наименование'
                        value={name}
                        onChange={(event) => {
                            setName(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                    <TextField
                        fullWidth
                        label='Кратко'
                        value={shortname}
                        onChange={(event) => {
                            setShortname(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                    {elemType.id == 1 && 
                        <EnhancedSelect
                            disabled={elem !== undefined}
                            name={'Группа'}
                            menuItems={branches}
                            setParentValue={setBranch_id}
                            defaultValue={branch_id}
                        />
                    }
                    {elemType.id == 3 &&
                        <>
                            <EnhancedSelect
                                name={'Форма контроля'}
                                menuItems={didacticForeignList['control_id']}
                                setParentValue={setControl}
                                defaultValue={control}
                            />
                            <EnhancedSelect
                                name={'Формат дисциплины'}
                                menuItems={didacticForeignList['discform_id']}
                                setParentValue={setDiscform}
                                defaultValue={discform}
                            />
                            <EnhancedSelect
                                disabled={elem !== undefined}
                                name={'Базовая дисциплина'}
                                menuItems={disciplines}
                                setParentValue={setDiscipline_id}
                                defaultValue={discipline_id}
                            />
                        </>
                    }

                    {elemType.id == 5 &&
                        <>
                            <EnhancedSelect
                                name={'Форма контроля'}
                                menuItems={didacticForeignList['control_id']}
                                setParentValue={setControl}
                                defaultValue={control}
                            />
                            <Didactic
                                didactic={didactic}
                                didacticForeignList={didacticForeignList}
                                setDidactic={setDidactic}
                                didacticFields={didacticFieldsList}
                                elem={elem}
                            />
                        </>
                    }
                    {elem !== undefined &&
                        <Connection
                            elem={elem}
                            reload={reload}
                            setReload={setReload}
                            multilevel
                        />
                    }
                </Grid>

                <Button
                    variant='contained'
                    sx={{ margin: 2 }}
                    onClick={handleSave}>
                    Сохранить
                </Button>
                <Button
                    variant='contained'
                    sx={{ margin: 2 }}
                    onClick={handleClose}>
                    Отменить
                </Button>
            </DialogContent>
        </Dialog>
    )
}
