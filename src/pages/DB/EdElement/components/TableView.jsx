import {useState, useEffect} from 'react'
import axios from '../../../../utils/axios'
import SkeletonTable from '../../../../components/Skeleton/Table'
import Table from '@mui/material/Table'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TablePagination from '@mui/material/TablePagination'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import EditIcon from '@mui/icons-material/Edit'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import {Delete} from '@mui/icons-material'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import {pink} from '@mui/material/colors'
import AddTaskIcon from '@mui/icons-material/AddTask'
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd'
import AddLinkIcon from '@mui/icons-material/AddLink'
import Methods from './Methods'
import Tasks from './Tasks'
import Questions from './Questions'
import AddE from './AddE'
import DelE from './DelE'
import DialogDidactic from './DialogDidactic'
import AddC from '../../Competence/components/AddC'
import DelC from '../../Competence/components/DelC'
import Competences from './Competences'
import QuestionMarkIcon from '@mui/icons-material/QuestionMark'

export function TableView({
    type,
    oneItemType,
    parentType_id,
    columnList,
    edElementTypeList,
    didacticForeignList,
    didacticFieldsList,
}) {
    const [state, setState] = useState({
        page: 0,
        perPage: 15,
        items: [],
        total: 0,
        loading: true,
        completed: false,
    })

    const [prevParIDs, setPrevParIDs] = useState([
        {
            parentName: null,
            parentId: null,
            level: -1,
            branch_id: null,
            discipline_id: null,
        },
    ])

    //dialog states
    const [add, setAdd] = useState(false)
    const [edit, setEdit] = useState(false)
    const [del, setDel] = useState(false)
    //edelement dialog states
    const [openDidact, setOpenDidact] = useState(false)
    const [openMethod, setOpenMethod] = useState(false)
    const [openTask, setOpenTask] = useState(false)
    const [openQuestion, setOpenQuestion] = useState(false)
    const [openCompetence, setOpenCompetence] = useState(false)
    //other states
    const [addElemType, setAddElemType] = useState('')
    const [addElem, setAddElem] = useState()
    const [reload, setReload] = useState(false)
    const [currentName, setCurrentName] = useState(null)
    const [currentId, setCurrentId] = useState(0)
    const [currentElemType, setCurrentElemType] = useState(
        type === 'edelements' && edElementTypeList[0]
    )
    const [name, setName] = useState('')
    const [order, setOrder] = useState(null)
    const [didactic, setDidactic] = useState()
    const [elem, setElem] = useState()
    const [discipline, setDiscipline] = useState()
    useEffect(() => {
        async function load(){
            await axios
            .post(`/api/${type}`, {
                perPage: state.perPage,
                parent_id: prevParIDs[prevParIDs.length - 1].parentId,
            })
            .then(({data}) => {
                if (data.total !== 0) {
                    let sortedData = data.items.sort(
                        (x, y) => x.order - y.order
                    )
                    setState((prev) => ({
                        ...prev,
                        page: 0,
                        items: sortedData,
                        total: data.total,
                        loading: false,
                        completed: data.lastPage,
                    }))
                    if (elem) {
                        setElem(sortedData.find(a => a.id == elem.id))
                    }
                    setCurrentName(prevParIDs[prevParIDs.length - 1].parentName)
                } else {
                    //возможно применение для загрузки подмножества
                    //подмножество может оказаться пустым
                    if (prevParIDs.length == 0) {
                        setAddElemType(edElementTypeList[0])
                        setAdd(true)
                    } else {
                        let p = prevParIDs
                        p.pop()
                        setPrevParIDs(p)
                        setReload(!reload)
                        if (type === 'edelements')
                            setCurrentElemType(
                                edElementTypeList[currentElemType.id - 2]
                            )
                    }
                }
            })
        }
        load()
    }, [reload, parentType_id])

    useEffect(() => {
        if (order == -1) {
            axios
                .get(
                    `/api/${oneItemType}/${addElem.parentId}/childrencount`,
                    {}
                )
                .then(({data}) => {
                    setOrder(data + 1)
                })
        }
    }, [add])

    const handleChangePage = (_, newPage) => {
        if (
            state.loading ||
            newPage < 0 ||
            newPage * state.perPage >= state.total
        )
            return

        setState((prev) => ({...prev, page: newPage}))

        if (state.completed) return

        setState((prev) => ({...prev, loading: true}))

        axios
            .post(`/api/${type}`, {
                perPage: state.perPage,
                pageCount: newPage + 1,
                parent_id: prevParIDs[prevParIDs.length - 1].parentId,
            })
            .then(({data}) => {
                setState((prev) => ({
                    ...prev,
                    items: [...prev.items, ...data.items],
                    loading: false,
                    completed: data.lastPage,
                }))
            })
    }

    const handleChangeRowsPerPage = (event) => {
        const newPerPage = parseInt(event.target.value)

        setState((prev) => ({
            ...prev,
            perPage: newPerPage,
            loading: true,
        }))

        axios
            .post(`/api/${type}`, {
                perPage: newPerPage,
                parent_id: prevParIDs[prevParIDs.length - 1].parentId,
            })
            .then(({data}) => {
                setState((prev) => ({
                    ...prev,
                    items: data.items,
                    loading: false,
                    completed: data.lastPage,
                }))
            })
    }

    let headRow = []
    columnList.map((col) => {
        headRow.push(
            <TableCell key={col} sx={{fontSize: 'h6.fontSize'}} width='auto'>
                {col}
            </TableCell>
        )
    })

    const bodyRows = state.loading ? (
        <SkeletonTable rows={state.perPage} columns={columnList.length} />
    ) : (
        state.items
            .slice(
                state.page * state.perPage,
                state.page * state.perPage + state.perPage
            )
            .map((elem) => (
                <TableRow
                    key={'pt-' + elem.name}
                    onClick={() => {
                        if (
                            type === 'edelements' &&
                            elem.edelementtype_id == 5
                        ) {
                            setOpenDidact(true)
                            setDidactic(elem.didacticdescription)
                            setElem(elem)
                        } else {
                            let p = prevParIDs
                            p.push({
                                parentName: elem.name,
                                parentId: elem.id,
                                level: elem.level,
                                branch_id: elem.branch_id,
                                discipline_id: elem.discipline_id,
                            })
                            setPrevParIDs(p)
                            setReload(!reload)
                            if (type === 'edelements')
                                setCurrentElemType(
                                    edElementTypeList[currentElemType.id]
                                )
                            if (elem.edelementtype_id == 3) setDiscipline(elem)
                        }
                    }}
                    sx={{'&:hover': {background: '#f1f1f1'}}}>
                    {/**----------------------------------------------------------------------*/}
                    {type === 'edelements' && (
                        <>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.edelementtype_id === 5
                                    ? elem.didacticdescription.packnumber + ' '
                                    : null}
                                {elem.name}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {currentName}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.shortname}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.control_id !== null
                                    ? didacticForeignList.control_id[
                                          elem.control_id - 1
                                      ].name
                                    : null}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.discform_id !== null
                                    ? didacticForeignList.discform_id[
                                          elem.discform_id - 1
                                      ].name
                                    : null}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {
                                    edElementTypeList[elem.edelementtype_id - 1]
                                        .name
                                }
                            </TableCell>
                        </>
                    )}
                    {type === 'competences' && (
                        <>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.name}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {currentName}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.code}
                            </TableCell>
                            <TableCell sx={{fontSize: 'h6.fontSize'}}>
                                {elem.shortname}
                            </TableCell>
                        </>
                    )}
                    {/**----------------------------------------------------------------------*/}
                    <TableCell sx={{fontSize: 'h6.fontSize'}}>
                        <Tooltip title='Отредактировать'>
                            <IconButton
                                aria-label='settings'
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setName(elem.name)
                                    setElem(elem)
                                    setCurrentId(elem.id)
                                    setAddElem(
                                        prevParIDs[prevParIDs.length - 1]
                                    )
                                    if (type === 'edelements')
                                        setAddElemType(
                                            edElementTypeList[
                                                elem.edelementtype_id - 1
                                            ]
                                        )
                                    setEdit(true)
                                }}
                                disabled={edit}>
                                <EditIcon sx={{fontSize: 40}} />
                            </IconButton>
                        </Tooltip>

                        <Tooltip title='Новый подтип'>
                            <IconButton
                                aria-label='settings'
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setAddElem({
                                        parentName: elem.name,
                                        parentId: elem.id,
                                        level: elem.level,
                                        branch_id: elem.branch_id,
                                        discipline_id: elem.discipline_id,
                                    })
                                    if (type === 'edelements')
                                        setAddElemType(
                                            edElementTypeList[
                                                elem.edelementtype_id
                                            ]
                                        )
                                    setOrder(-1)
                                    setAdd(true)
                                }}>
                                <AddIcon color='success' sx={{fontSize: 40}} />
                            </IconButton>
                        </Tooltip>

                        <Tooltip title='Удалить'>
                            <IconButton
                                aria-label='settings'
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setDel(true)
                                    setCurrentId(elem.id)
                                }}
                                disabled={del}>
                                <Delete sx={{color: pink[500], fontSize: 40}} />
                            </IconButton>
                        </Tooltip>
                        {/**------------------------------------------------------------------------------------------ */}
                        {(elem.edelementtype_id == 4 ||
                            elem.edelementtype_id == 5 ||
                            elem.edelementtype_id == 3) && (
                            <Tooltip title='Связь с вопросами'>
                                <IconButton
                                    aria-label='settings'
                                    onClick={(event) => {
                                        event.stopPropagation()
                                        setOpenQuestion(true)
                                        setElem(elem)
                                        setCurrentId(elem.id)
                                    }}>
                                    <QuestionMarkIcon
                                        sx={{
                                            color: pink[500],
                                            fontSize: 20,
                                        }}
                                    />
                                </IconButton>
                            </Tooltip>
                        )}
                        {type === 'edelements' &&
                            elem.edelementtype_id == 5 && (
                                <>
                                    <Tooltip title='Методы'>
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                event.stopPropagation()
                                                setOpenMethod(true)
                                                setElem(elem)
                                                setCurrentId(elem.id)
                                            }}>
                                            <AddTaskIcon
                                                sx={{
                                                    fontSize: 20,
                                                }}
                                            />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title='Задачи'>
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                event.stopPropagation()
                                                setOpenTask(true)
                                                setElem(elem)
                                                setCurrentId(elem.id)
                                            }}>
                                            <AddTaskIcon
                                                sx={{
                                                    color: pink[500],
                                                    fontSize: 20,
                                                }}
                                            />
                                        </IconButton>
                                    </Tooltip>

                                    <Tooltip title='Компетенции'>
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                event.stopPropagation()
                                                setOpenCompetence(true)
                                                setElem(elem)
                                                setCurrentId(elem.id)
                                            }}>
                                            <AddLinkIcon
                                                sx={{
                                                    fontSize: 20,
                                                }}
                                            />
                                        </IconButton>
                                    </Tooltip>
                                </>
                            )}
                        {/**------------------------------------------------------------------------------------------ */}
                    </TableCell>
                </TableRow>
            ))
    )
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <TableContainer component={Paper}>
                    <Table sx={{minWidth: 400}}>
                        <TableHead>
                            <TableRow>
                                {headRow}
                                <TableCell width='20%'>
                                    <Tooltip title='Новый тип'>
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                event.stopPropagation()
                                                setAddElem(
                                                    prevParIDs[
                                                        prevParIDs.length - 1
                                                    ]
                                                )
                                                if (type === 'edelements')
                                                    setAddElemType(
                                                        currentElemType
                                                    )
                                                setAdd(true)
                                                setOrder(state.total + 1)
                                            }}>
                                            <AddIcon
                                                sx={{fontSize: 40}}
                                                color='primary'
                                            />
                                        </IconButton>
                                    </Tooltip>
                                    {prevParIDs.length > 1 &&
                                        prevParIDs[prevParIDs.length - 1]
                                            .parentId !== null && (
                                            <Tooltip title='Вернуться на уровень выше'>
                                                <IconButton
                                                    aria-label='settings'
                                                    onClick={(event) => {
                                                        event.stopPropagation()
                                                        let p = prevParIDs
                                                        p.pop()
                                                        setPrevParIDs(p)
                                                        setReload(!reload)
                                                        if (
                                                            type ===
                                                            'edelements'
                                                        )
                                                            setCurrentElemType(
                                                                edElementTypeList[
                                                                    currentElemType.id -
                                                                        2
                                                                ]
                                                            )
                                                    }}>
                                                    <ArrowUpwardIcon
                                                        sx={{fontSize: 40}}
                                                        color='success'
                                                    />
                                                </IconButton>
                                            </Tooltip>
                                        )}
                                </TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>{bodyRows}</TableBody>
                    </Table>
                </TableContainer>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 15, 25]}
                    component='div'
                    count={state.total}
                    page={state.page}
                    rowsPerPage={state.perPage}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage='Строк:'
                    labelDisplayedRows={({from, to, count}) =>
                        `${from} - ${to} из ${count}`
                    }
                />
            </Grid>
            {/**------------------------------------------------------------------------------------------ */}
            {type === 'edelements' && edit ? (
                <AddE
                    add={edit}
                    setAdd={setEdit}
                    reload={reload}
                    setReload={setReload}
                    oneItemType={oneItemType}
                    parent={addElem}
                    parentType_id={parentType_id}
                    elemType={addElemType}
                    didacticForeignList={didacticForeignList}
                    elem={elem}
                    didacticFieldsList={didacticFieldsList}
                />
            ) : null}
            {type === 'edelements' && add ? (
                <AddE
                    add={add}
                    setAdd={setAdd}
                    reload={reload}
                    setReload={setReload}
                    oneItemType={oneItemType}
                    parent={addElem}
                    parentType_id={parentType_id}
                    elemType={addElemType}
                    order={order}
                    didacticForeignList={didacticForeignList}
                    didacticFieldsList={didacticFieldsList}
                />
            ) : null}
            {type === 'edelements' && del ? (
                <DelE
                    name={name}
                    id={currentId}
                    reload={reload}
                    setReload={setReload}
                    del={del}
                    setDel={setDel}
                    oneItemType={oneItemType}
                />
            ) : null}
            {type === 'competences' && edit ? (
                <AddC
                    add={edit}
                    setAdd={setEdit}
                    reload={reload}
                    setReload={setReload}
                    oneItemType={oneItemType}
                    parent={addElem}
                    elem={elem}
                />
            ) : null}
            {type === 'competences' && add ? (
                <AddC
                    add={add}
                    setAdd={setAdd}
                    reload={reload}
                    setReload={setReload}
                    oneItemType={oneItemType}
                    parent={addElem}
                    order={order}
                />
            ) : null}
            {type === 'competences' && del ? (
                <DelC
                    name={name}
                    id={currentId}
                    reload={reload}
                    setReload={setReload}
                    del={del}
                    setDel={setDel}
                    oneItemType={oneItemType}
                />
            ) : null}
            {openDidact ? (
                <DialogDidactic
                    open={openDidact}
                    setOpen={setOpenDidact}
                    didactic={didactic}
                    elem={elem}
                    didacticFields={didacticFieldsList}
                    didacticForeignList={didacticForeignList}
                    disabled
                    reload={reload}
                    setReload={setReload}
                    discipline={discipline}
                />
            ) : null}
            {openMethod ? (
                <Methods
                    open={openMethod}
                    setOpen={setOpenMethod}
                    elem={elem}
                    reload={reload}
                    setReload={setReload}
                />
            ) : null}
            {openTask ? (
                <Tasks
                    open={openTask}
                    setOpen={setOpenTask}
                    elem={elem}
                    reload={reload}
                    setReload={setReload}
                />
            ) : null}
            {openQuestion ? (
                <Questions
                    open={openQuestion}
                    setOpen={setOpenQuestion}
                    elem={elem}
                    reload={reload}
                    setReload={setReload}
                />
            ) : null}
            {openCompetence ? (
                <Competences
                    open={openCompetence}
                    setOpen={setOpenCompetence}
                    elem={elem}
                    reload={reload}
                    setReload={setReload}
                />
            ) : null}
            {/**------------------------------------------------------------------------------------------ */}
        </Grid>
    )
}
