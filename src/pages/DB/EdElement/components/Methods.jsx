import { useState, useEffect } from 'react'
import axios from '../../../../utils/axios'
import { Dialog, DialogContent, DialogTitle, Grid } from '@mui/material'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
export default function ({ open, setOpen, elem, reload, setReload }) {
    const handleClose = () => {
        setOpen(false)
    }

    const [methods, setMethods] = useState()

    useEffect(() => {
        axios
            .post(`/api/methods`, {
                perPage: 1000,
            })
            .then(({ data }) => {
                let m = []
                data.items.map(method => {
                    let check = false

                    if (elem.methods.find(({ id }) => id == method.id))
                        check = true
                    m.push({
                        id: method.id,
                        name: method.name,
                        checked: check,
                    })
                })
                setMethods(m)
            })
    }, [])

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                Методы {elem.name}
            </DialogTitle>
            <DialogContent>
                <Grid container>
                    <Grid item xs={12}>
                        {methods !== undefined ? (
                            <CheckboxList
                                list={methods}
                                edelement_id={elem.id}
                                reload={reload}
                                setReload={setReload}
                            />
                        ) : null}
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
}

function CheckboxList({ list, edelement_id, reload, setReload }) {
    const [state, setState] = useState(list)
    const [checked, setChecked] = useState({
        id: -1,
        checked: false,
    })

    useEffect(() => {
        if (checked.id !== -1) {
            /**
             * добавление связи
             */
            if (checked.checked === true) {
                axios
                    .post(`/api/edelementlinkmethod/`, {
                        edelement_id: edelement_id,
                        method_id: checked.id,
                    })
                    .then(({ data }) => {
                        console.log(data)
                    })
            }
            /**
             * удаление связи
             */
            if (checked.checked === false) {
                axios
                    .delete(
                        `/api/edelementlinkmethod?edelement_id=${edelement_id}&method_id=${checked.id}`,
                        {},
                    )
                    .then(({ data }) => {
                        console.log(data)
                    })
            }
            setReload(!reload)
        }
    }, [checked])

    const handleChange = event => {
        let newArr = []
        state.map(item => {
            newArr.push(item)
            if (event.target.name === String(item.id)) {
                newArr[newArr.length - 1].checked = event.target.checked
                setChecked({ id: item.id, checked: item.checked })
            }
        })
        setState(newArr)
    }

    return (
        <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
            {state.map(item => {
                const labelId = `checkbox-list-label-${item.id}`

                return (
                    <ListItem
                        key={item.id}
                        disablePadding
                        secondaryAction={
                            <Checkbox
                                name={String(item.id)}
                                edge="start"
                                checked={item.checked}
                                onChange={handleChange}
                                inputProps={{ 'aria-labelledby': labelId }}
                            />
                        }
                    >
                        <ListItemText id={labelId} primary={item.name} />
                    </ListItem>
                )
            })}
        </List>
    )
}
