import * as React from 'react'
import { TableView } from './components/TableView'
import { Typography, Paper } from '@mui/material'
import axios from '../../../utils/axios'

export default function () {
    let columnList = [
        'Наименование',
        'Надэлемент',
        'Кратко',
        'Форма контроля',
        'Формат дисциплины',
        'Тип элемента',
    ]

    let didacticFieldsList = [
        {
            name: 'Вид',
            type: 'type_id',
            input: 'Select',
        },
        {
            name: 'Важность',
            type: 'importance',
            input: 'TextField',
        },
        {
            name: 'Сложность',
            type: 'difficulty',
            input: 'TextField',
        },
        {
            name: 'Ядро',
            type: 'core_id',
            input: 'Select',
        },
        {
            name: 'Форма представления',
            type: 'viewtype',
            input: 'TextField',
        },
        {
            name: 'Номер',
            type: 'packnumber',
            input: 'TextField',
        },
        {
            name: 'Нормативы',
            type: 'norms',
            input: 'TextField',
        },
        {
            name: 'Профиль',
            type: 'profile',
            input: 'TextField',
        },
    ]

    const [types, setTypes] = React.useState(null)
    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios.get(`/api/edelement/didactic/wbs`, {}).then(({ data }) => {
            setTypes(data)
        })
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])

    if (show && types !== null)
        return (
            <>
                <Typography variant='h5' sx={{ textAlign: 'center', m: 2 }}>
                    Образовательные элементы
                </Typography>
                <TableView
                    oneItemType={'edelement'}
                    type={'edelements'}
                    columnList={columnList}
                    edElementTypeList={types.edelementtype}
                    didacticForeignList={{
                        control_id: types.control,
                        core_id: types.core,
                        type_id: types.type,
                        discform_id: types.discform,
                    }}
                    didacticFieldsList={didacticFieldsList}
                />
            </>
        )
}
