import * as React from 'react'
import { TableView } from '../EdElement/components/TableView'
import { Typography, Paper } from '@mui/material'
import axios from '../../../utils/axios'

export default function () {
    let columnList = ['Наименование', 'Надэлемент', 'Шифр', 'Кратко']

    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])
    if (show)
        return (
            <>
                <Typography variant='h5' sx={{ textAlign: 'center', m: 2 }}>
                    Компетенции
                </Typography>
                <TableView
                    oneItemType={'competence'}
                    type={'competences'}
                    columnList={columnList}
                />
            </>
        )
}
