import { useState } from 'react'
import axios from '../../../../utils/axios'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import { Dialog, DialogContent, DialogTitle } from '@mui/material'
export default function ({
    add,
    setAdd,
    parent,
    setReload,
    reload,
    oneItemType,
    order,
    elem, //редактирование; undefined - добавление, {...} - редактирование
}) {
    console.log(oneItemType)
    const [name, setName] = useState(elem !== undefined ? elem.name : '')
    const [shortname, setShortname] = useState(
        elem !== undefined ? elem.shortname : '',
    )
    const [code, setCode] = useState(elem !== undefined ? elem.code : '')
    const [open, setOpen] = useState(add)
    const handleClose = () => {
        setOpen(false)
        setAdd(!add)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}
            >
                {elem !== undefined ? 'Редактировать' : 'Добавить'}
            </DialogTitle>
            <DialogContent>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        label="Надэлемент"
                        value={parent != undefined ? parent.parentName : 'нет'}
                        sx={{ mb: 2 }}
                        disabled
                    />
                    <TextField
                        fullWidth
                        label="Наименование"
                        value={name}
                        onChange={event => {
                            setName(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                    <TextField
                        fullWidth
                        label="Кратко"
                        value={shortname}
                        onChange={event => {
                            setShortname(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                    <TextField
                        fullWidth
                        label="Шифр"
                        value={code}
                        onChange={event => {
                            setCode(event.target.value)
                        }}
                        sx={{ mb: 2 }}
                    />
                </Grid>

                <Button
                    variant="contained"
                    sx={{ margin: 2 }}
                    onClick={event => {
                        setOpen(!open)
                        setAdd(!add)
                        console.log(parent)
                        if (elem !== undefined)
                            axios
                                .put(`/api/${oneItemType}/${elem.id}`, {
                                    name: name,
                                    shortname: shortname,
                                    code: code,
                                })
                                .then(({ data }) => {
                                    console.log(data)
                                })
                        else
                            axios
                                .post(`/api/${oneItemType}/create`, {
                                    name: name,
                                    shortname: shortname,
                                    code: code,
                                    order: order,
                                    level: parent.level + 1,
                                    parent_id: parent.parentId,
                                })
                                .then(({ data }) => {
                                    console.log(data)
                                })

                        setReload(!reload)
                    }}
                >
                    Сохранить
                </Button>
                <Button
                    variant="contained"
                    sx={{ margin: 2 }}
                    onClick={event => {
                        setOpen(!open)
                        setAdd(!add)
                    }}
                >
                    Отменить
                </Button>
            </DialogContent>
        </Dialog>
    )
}
