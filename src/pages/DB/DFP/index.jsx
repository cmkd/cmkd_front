import * as React from 'react'
import axios from '../../../utils/axios'
import { Button, Grid } from '@mui/material'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import EnhancedSelect from '../../../components/EnhancedSelect'
import 'dayjs/locale/ru'
import dayjs from 'dayjs'
import Papa from 'papaparse'

export default function () {

    const [show, setShow] = React.useState(false)
    const [to, setTo] = React.useState(null)
    const [from, setFrom] = React.useState(null)
    const [profile, setProfile] = React.useState()
    const [profiles, setProfiles] = React.useState([])
    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
        axios
            .post(`/api/user/profiles?perPage=999&pageCount=1`,{})
            .then(({data}) => {
                console.log(data)
                let t = []
                data.items.forEach(profile => {
                    t.push({
                        id: profile.id,
                        name: profile.user.surname + ' ' + profile.user.name + ' ' + profile.user.patronym,
                    })
                });
                setProfiles(t)
            })
    }, [])

    const handleClick = async () => {
        if (to && from && profile)
            await axios
                .post(`/api/${profile}/dfp`,{
                    dateUp: to,
                    dateDown: from,
                })
                .then(({data}) => {
                    if (data.length > 0){
                        data.forEach(element => {
                            const timestamp = Date.parse(element.created_at)
                            const date = new Date(timestamp)
                            const locDate = date.toLocaleDateString('ru-RU', {timeZone: 'UTC'})
                            const locTime = date.toLocaleTimeString('ru-RU', {timeZone: 'UTC'})
                            console.log(locDate, locTime)
                            element.created_at = locDate + ' ' + locTime
                            element.date = locDate
                            element.time = locTime
                        });
                        const csv = 'sep=;\n' + Papa.unparse(data, {delimiter: ';'})
                        let blob = new Blob([
                            encodeCP1251(csv)
                        ], {
                            type: 'text/csv;charset=windows-1251',
                        })
                        var link = document.createElement("a");
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href",  "data:text/plain;charset=CP1251," + encodeCP1251(csv));
                        link.setAttribute("download", "results.csv");
                        document.body.appendChild(link);
                        link.click();
                        setTimeout(function() {document.body.removeChild(link); window.URL.revokeObjectURL(url); }, 0); 
                    }
                })
    }

    if (show)
        return (
            <Grid container>
                <Grid item xs={12}>
                    <EnhancedSelect
                        key={'Профили'}
                        name={'Профили'}
                        menuItems={profiles}
                        setParentValue={setProfile}
                    />
                </Grid>
                <Grid item xs={12}>
                    <LocalizationProvider
                        dateAdapter={AdapterDayjs}
                        adapterLocale={'ru'}
                    >
                        <DesktopDatePicker
                            sx={{ mb: 2 }}
                            label={'От'}
                            value={dayjs(from)}
                            mask={'__.__.____'}
                            onChange={(value) => {
                                setFrom(value)
                            }}
                            renderInput={(params) => (
                                <TextField {...params} fullWidth />
                            )}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12}>
                    <LocalizationProvider
                        dateAdapter={AdapterDayjs}
                        adapterLocale={'ru'}
                    >
                        <DesktopDatePicker
                            sx={{ mb: 2 }}
                            label={'До'}
                            value={dayjs(to)}
                            mask={'__.__.____'}
                            onChange={(value) => {
                                setTo(value)
                            }}
                            renderInput={(params) => (
                                <TextField {...params} fullWidth />
                            )}
                        />
                    </LocalizationProvider>
                </Grid>
                <Button onClick={handleClick}>Скачать в CSV</Button>
            </Grid>
        )
}

var encodeCP1251 = function (string) {
    function encodeChar(c) {
        var isKyr = function (str) {
            return /[а-я]/i.test(str);
        }
        var cp1251 = 'ЂЃ‚ѓ„…†‡€‰Љ‹ЊЌЋЏђ‘’“”•–—�™љ›њќћџ ЎўЈ¤Ґ¦§Ё©Є«¬*®Ї°±Ііґµ¶·\
ё№є»јЅѕїАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя'; 
        var p = isKyr(c) ? (cp1251.indexOf(c) + 128) : c.charCodeAt(0);
        var h = p.toString(16);
        if (h=='a'){
            h = '0A';
        }
        if (h != 'd')
        return '%' + h;
        else return ''
    }
    var res = '';
    for (var i = 0; i < string.length; i++) { 
        res += encodeChar(string.charAt(i)) //ну или string[i]
    }
    return res;
}