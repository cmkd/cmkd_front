import * as React from 'react'
import { Typography, Paper, Grid } from '@mui/material'
import ItemList from '../../../components/ItemList'
import Upload from './components/Upload'
import axios from '../../../utils/axios'
export default function () {
    const [openFileUpload, setOpenFileUpload] = React.useState(false)
    const [show, setShow] = React.useState(false)

    React.useEffect(() => {
        axios
            .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
            .then(({ data }) => {
                if (
                    !data.permissions.some((item) => {
                        return item.id == 1 || item.id == 2 || item.id == 4
                    })
                )
                    window.location.replace(`/lms/`)
                else setShow(true)
            })
    }, [])
    if (show)
        return (
            <Grid container>
                <Grid item xs={10}>
                    <Typography variant='h5' sx={{ textAlign: 'center', m: 2 }}>
                        Файлы
                    </Typography>
                </Grid>
                <Upload open={openFileUpload} setOpen={setOpenFileUpload} />
                <ItemList
                    type={'filestorage'}
                    types={'files'}
                    setAddAction={setOpenFileUpload}
                />
            </Grid>
        )
}
