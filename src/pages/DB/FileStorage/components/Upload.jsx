import { useRef, useState } from 'react'
import axios from '../../../../utils/axios'
import {
    Dialog,
    DialogContent,
    DialogTitle,
    Grid,
    Button,
    Typography,
} from '@mui/material'

export default function ({ open, setOpen }) {
    const inputFile = useRef(null)

    const handleClose = () => {
        setOpen(false)
        location.reload()
    }

    const [file, setFile] = useState(null)

    const onButtonClick = () => {
        // `current` points to the mounted file input element
        inputFile.current.click()
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            maxWidth={'lg'}
            fullWidth
            aria-labelledby='scroll-dialog-title'
            aria-describedby='scroll-dialog-description'>
            <DialogTitle
                sx={{
                    fontSize: 'h5.fontSize',
                    textAlign: 'center',
                }}>
                Загрузка файла
            </DialogTitle>
            <DialogContent>
                <input
                    type='file'
                    id='file'
                    ref={inputFile}
                    style={{ display: 'none' }}
                    onChange={() => {
                        console.log(event.target.files[0])
                        setFile(event.target.files[0])
                    }}
                />
                <Button onClick={onButtonClick}>Выбрать файл</Button>

                {file !== null && (
                    <>
                        <Typography>{file.name}</Typography>
                        <Button
                            onClick={() => {
                                console.log(file)
                                const formData = new FormData()
                                formData.append('file', file)
                                formData.append('name', file.name)
                                const config = {
                                    headers: {
                                        'content-type': 'multipart/form-data',
                                    },
                                }
                                axios
                                    .post(`/api/filestorage`, formData, config)
                                    .then(({ data }) => {
                                        console.log(data)
                                    })
                            }}>
                            Загрузить файл
                        </Button>
                    </>
                )}
            </DialogContent>
        </Dialog>
    )
}
