import React from 'react'
import {
    Grid,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    Button,
} from '@mui/material'
import ItemCard from '../../components/ItemCard'
import axios from '../../utils/axios'
import {useNavigate} from 'react-router-dom'
import { dfp } from '../../utils/dfp'

export default function () {
    const navigate = useNavigate()
    const [plan, setPlan] = React.useState(0)
    const [userProfileID, setUserProfileID] = React.useState()
    const [userCourses, setUserCourses] = React.useState([])
    const [branchCourses, setBranchCourses] = React.useState([])
    const [passedCourses, setPassedCourses] = React.useState([])
    const [endLoading, setEndLoading] = React.useState(false)
    const [userActivity, setUserActivity] = React.useState(null)
    if (localStorage.getItem('_token') == null)
        window.location.replace('/lms/auth')
    React.useEffect(() => {
        async function download() {
            await axios.post(`/api/user/authprofile/`, {}).then(({data}) => {
                setPlan(data.edelement_id)
                setUserProfileID(data.id)
                setUserCourses(data.usereducationcourses)
            })
        }
        if (localStorage.getItem('_token') != null) download()
    }, [])

    React.useEffect(() => {
        async function download() {
            await axios
                .get(`/api/user/profile/${userProfileID}/courses`, {})
                .then(({data}) => {
                    setBranchCourses(data)
                })
        }
        if (userProfileID) download()
    }, [userProfileID])

    React.useEffect(() => {
        if (branchCourses.length > 0 && !endLoading) {
            let active = userCourses,
                passed = [],
                all = branchCourses,
                t = []

            all.forEach((course) => {
                if (!active.some((c) => c.educationcourse.id == course.id))
                    t.push(
                        <Grid item xs={12}>
                            <ItemCard
                                firstString={course.name}
                                secondString={course.about}
                                sx={{m: 1}}
                                cardID={course.id}
                                cardType={'free'}
                                objCard={course}
                                setParentValue={setUserActivity}
                            />
                        </Grid>
                    )
            })
            all = t
            t = []
            active.forEach((course) => {
                if (course.finished == 1)
                    passed.push(
                        <Grid item xs={12}>
                            <ItemCard
                                firstString={course.educationcourse.name}
                                secondString={course.educationcourse.about}
                                sx={{m: 1}}
                                cardID={course.id}
                                cardType={'passed'}
                                objCard={course}
                                setParentValue={setUserActivity}
                            />
                        </Grid>
                    )
                else
                    t.push(
                        <Grid item xs={12}>
                            <ItemCard
                                firstString={course.educationcourse.name}
                                secondString={course.educationcourse.about}
                                sx={{m: 1}}
                                cardID={course.id}
                                cardType={'active'}
                                objCard={course}
                                setParentValue={setUserActivity}
                            />
                        </Grid>
                    )
            })
            active = t

            //-----------------------------------------------------------------------
            setEndLoading(true)
            setUserCourses(active)
            setBranchCourses(all)
            setPassedCourses(passed)
        }
    }, [userCourses, branchCourses])

    const handleAcceptSubscribe = async () => {
        await axios
            .post(`/api/user/course`, {
                usereducationprofile_id: userProfileID,
                educationcourse_id: userActivity.ID,
                score: 0,
                finished: 0,
            })
            .then(({data}) => {
                console.log(data)
            })
        console.log(userActivity)
        dfp(
            userProfileID,
            'Главная страница',
            'Анкетирование по ' + userActivity.objCard.name,
            'Запись на курс ' + userActivity.objCard.name,
            'Успешно',
            'Состоялась запись на курс, учащийся отправлен на анкетирование для выявления потребностей и приоритетов'
        )
        await axios
            .get(
                `api/discipline/${userActivity.objCard.edelement.discipline_id}/tests?option=0`,
                {}
            )
            .then(({data}) => {
                navigate(`/lms/test`, {
                    state: {
                        course_id: userActivity.ID,
                        usereducationprofile_id: userProfileID,
                        test_id: data.id,
                        edelement_id: '',
                        edelement: null,
                    },
                })
            })
    }

    const handleDeclineSubscribe = () => {
        setUserActivity(null)
        dfp(
            userProfileID,
            'Главная страница',
            '-',
            'Запись на курс',
            'Отмена пользователем',
            '',
        )
    }

    React.useEffect(() => {
        async function test() {
            await axios
                .get(
                    `api/discipline/${userActivity.objCard.educationcourse.edelement.discipline_id}/tests?option=1`,
                    {}
                )
                .then(({data}) => {
                    navigate(`/lms/test`, {
                        state: {
                            course_id: userActivity.ID,
                            usereducationprofile_id: userProfileID,
                            test_id: data.id,
                            edelement_id: userActivity.objCard.educationcourse.edelement.id,
                            edelement: userActivity.objCard.educationcourse.edelement,
                        },
                    })
                })
        }

        if (
            userActivity != null &&
            userActivity.type == 'active' &&
            userActivity.objCard.score == 0
        ) {
            dfp(
                userProfileID,
                'Главная страница',
                'Вступительный тест по дисциплине ' + userActivity.objCard.educationcourse.name,
                'Карточка курса',
                'Выбор пользователем',
                'Учащийся отправлен на вступительное тестирование с целью выявления уровня знаний',
            )
            test()
        } else if (userActivity != null && userActivity.type == 'active') {
            dfp(
                userProfileID,
                'Главная страница',
                'Курс по дисциплине',
                'Карточка курса',
                'Выбор пользователем',
                'Учащийся зашёл в оглавление курса ' + userActivity.objCard.educationcourse.name,
            )
            navigate(`/lms/course`, {
                state: {
                    plan_id: plan,
                    course_id: userActivity.ID,
                    usereducationprofile_id: userProfileID,
                },
            })
        }
    }, [userActivity])

    if (endLoading)
        return (
            <Grid container>
                <Grid item xs={3}></Grid>
                <Grid item xs={6} textAlign={'center'} sx={{mt: 3, mb: 5}}>
                    <Typography variant='h5'>
                        Добро пожаловать на LMS AESU!
                    </Typography>
                </Grid>
                <Grid item xs={3}></Grid>
                <Grid item xs={12}>
                    <Typography>
                        Данная часть портала AESU посвящена реализации системы
                        электронного курса. Здесь вы сможете обратиться к
                        доступным для вас электронным курсам, пройти необходимое
                        тестирование в ходе обучения и получить помощь в
                        процессе вашей образовательной деятельности.
                    </Typography>
                </Grid>
                {userCourses.length > 0 &&
                    <>
                        <Grid item xs={12}>
                            <Typography variant='h6'>Активные курсы</Typography>
                        </Grid>
                        {userCourses}
                    </>
                }
                {branchCourses.length > 0 &&
                    <>
                        <Grid item xs={12}>
                            <Typography variant='h6'>
                                Доступные к выбору курсы
                            </Typography>
                        </Grid>
                        {branchCourses}
                    </>
                }
                {passedCourses.length > 0 &&
                    <>
                        <Grid item xs={12}>
                            <Typography variant='h6'>
                                Пройденные курсы
                            </Typography>
                        </Grid>
                        {passedCourses}
                    </>
                }
                {userActivity != null && userActivity.type == 'free' &&
                    <Dialog
                        open={userActivity}
                        scroll={'paper'}
                        fullWidth
                        aria-labelledby='scroll-dialog-title'
                        aria-describedby='scroll-dialog-description'>
                        <DialogTitle
                            sx={{
                                fontSize: 'h5.fontSize',
                                textAlign: 'center',
                            }}>
                            Записаться на курс? После записи вы будете
                            перенаправлены на заполнение вступительной анкеты по
                            дисциплине
                        </DialogTitle>
                        <DialogContent>
                            <Grid container>
                                <Grid item xs={6} textAlign={'center'}>
                                    <Button
                                        variant='contained'
                                        onClick={handleAcceptSubscribe}>
                                        Да
                                    </Button>
                                </Grid>
                                <Grid item xs={6} textAlign={'center'}>
                                    <Button
                                        variant='contained'
                                        onClick={handleDeclineSubscribe}>
                                        Нет
                                    </Button>
                                </Grid>
                            </Grid>
                        </DialogContent>
                    </Dialog>
                }
            </Grid>
        )
    else
        return (
            <Typography>
                Для вас не предусмотрен образовательный профиль, обратитесь к
                научному руководителю
            </Typography>
        )
}
