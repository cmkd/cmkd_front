import { useState, useEffect } from 'react'

import Button from '@mui/material/Button'
import EnhancedSelect from '../../../../components/EnhancedSelect'
import axios from '../../../../utils/axios'
import drawOneLevelCMKD from '../../../../draw/drawOneLevelCMKD'
const pathList = [
    {
        name: 'Учебный план',
        type: 'plan',
        num: 1,
    },
    {
        name: 'Семестр',
        type: 'sem',
        num: 2,
    },
    {
        name: 'Дисциплина',
        type: 'disc',
        num: 3,
    },
]

export default function ({
    setLabels,
    setTree,
    setDrawInfo,
    setUpperInfo,
    setRedFlags,
}) {
    const [editedType, setEditedType] = useState('')
    const [edelement, setEdelement] = useState({
        plan: '',
        sem: '',
        disc: '',
        branch_id: 0,
        test_id: 0,
        discipline_id: 0,
    })
    const [edelementList, setEdelementList] = useState({
        plan: [],
        sem: [],
        disc: [],
    })
    const [baskets, setBaskets] = useState([])
    const [currentBasket, setCurrentBasket] = useState('')
    const [currentUser, setCurrentUser] = useState('')
    const [date, setDate] = useState('')

    useEffect(() => {
        //для создания нового соединения - загрузка учебных планов
        axios
            .post(`/api/edelements`, {
                parent_id: null,
            })
            .then(({ data }) => {
                if (data.total !== 0) {
                    let sortedData = data.items.sort(
                        (x, y) => x.order - y.order
                    )
                    setEdelementList((prev) => ({
                        ...prev,
                        plan: sortedData,
                    }))
                }
            })
    }, [])

    useEffect(() => {
        async function fetchData() {
            if (edelement.branch_id !== 0 && edelement.discipline_id !== 0)
                await axios
                    .post(`/api/baskets/models`, {
                        perPage: 1000,
                        discipline_id: edelement.discipline_id,
                        branch_id: edelement.branch_id,
                    })
                    .then(({ data }) => {
                        setBaskets(data.items)
                    })
        }
        fetchData()
    }, [edelement])

    useEffect(() => {
        //для создания нового соединения - загрузка остальных образ. элементов
        if (editedType !== 'disc') {
            let type
            let current = pathList.filter((obj) => {
                return obj.type === editedType
            })
            if (current[0] !== undefined) {
                let next = pathList.filter((obj) => {
                    return obj.num === current[0].num + 1
                })
                type = next[0].type
            }

            axios
                .post(`/api/edelements`, {
                    parent_id: edelement[editedType],
                })
                .then(({ data }) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order
                        )
                        setEdelementList((prev) => ({
                            ...prev,
                            [type]: sortedData,
                        }))
                    }
                })
        }
    }, [editedType, edelement])

    let addConnectionSelects = []
    pathList.map((step) => {
        addConnectionSelects.push(
            <EnhancedSelect
                name={step.name}
                type={step.type}
                menuItems={edelementList[step.type]}
                setParentValue={setEdelement}
                defaultValue={edelement[step.type]}
                setType={setEditedType}
                cmkd
            />
        )
    })
    useEffect(() => {
        let basket = baskets.find((elem) => elem.id == currentBasket)
        if (basket != undefined) {
            setDrawInfo((prev) => ({
                ...prev,
                competence_id: 0,
                user_id: basket.user_id,
                date: basket.passed_at,
                semester_id: edelement.sem,
                plan_id: edelement.plan,
                currentLoad_id: edelement.disc,
                lowestLevel: 4,
            }))
            setCurrentUser(basket.user_id)
            setDate(basket.passed_at)
        }
    }, [currentBasket])

    return (
        <>
            {addConnectionSelects}
            <EnhancedSelect
                name={'Тесты'}
                menuItems={baskets}
                setParentValue={setCurrentBasket}
                defaultValue={currentBasket}
                cmkd
            />

            {currentBasket !== '' && (
                <Button
                    variant='contained'
                    sx={{ mb: 2, maxWidth: 300 }}
                    onClick={() => {
                        console.log(currentUser, date)
                        drawOneLevelCMKD(
                            setLabels,
                            setTree,
                            edelement.disc, //87,
                            0,
                            4, //2,
                            currentUser,
                            date,
                            edelement.plan,
                            setRedFlags,
                            setUpperInfo
                        )
                    }}
                    fullWidth>
                    Нарисовать карту
                </Button>
            )}
        </>
    )
}
