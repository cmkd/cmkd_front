/**  */
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import * as React from 'react'
import {
    TextField,
    FormControl,
    Select,
    MenuItem,
    InputLabel,
    FormLabel,
    FormGroup,
    Checkbox,
    FormControlLabel,
    Link,
} from '@mui/material'

import Collapse from '@mui/material/Collapse'
import IconButton from '@mui/material/IconButton'
import ExpandCircleDownIcon from '@mui/icons-material/ExpandCircleDown'
import { styled } from '@mui/material/styles'
import PropTypes from 'prop-types'
import DrawCanvas from '../../../draw/DrawCanvas'
import TestCheck from './components/TestCheck'
//<a href="/1"></a>

const recomendations = [
    {
        name: 'Проблема №1',
        short: [
            'Проблема, возникшая с пониманием темы “Теория принятия решений” (t1), заключается в недостаточном освоении материалов «Теория принятия решений (ТПР). Терминология» (u2, ключевая дидактическая единица в курсе) и «Задача ТПР. Альтернативы и их классификация» (u3) зафиксированы по отношению к заданиям, проверяющим уровень развития компетенции “Способен анализировать профессиональную информацию, выделять в ней главное, структурировать, оформлять и представлять в виде аналитических обзоров с обоснованными выводами и рекомендациями” (OПК-3) (оценка уровня как «недостаточно развита»). От понимания материала u2 будет зависеть тема «База знаний. Инженерия знаний как деятельность и жизненный цикл ИИС» (u3) дисциплины «Системы искусственного интеллекта» (d6). ',
        ],
        full: '',
    },
    {
        name: 'Проблема №2',
        short: [
            'Проблема, возникшая с пониманием темы “Оперирование неопределенностями” (t3), заключается в недостаточном освоении материалов «Метод Монте-Карло» (u15, ключевая дидактической единицей в теме) является основой не только для последующего учебного материала (u16 и u17), но и необходима для освоения темы «Имитация псевдослучайных величин методом Монте-Карло в Python» (u24) дисциплины «Программная реализация математических моделей» (d2) и всей темы «Эволюционные алгоритмы» (t1.5) дисциплины «Интеллектуальный анализ данных» (d1), которые освоены недостаточно. ',
        ],
        full: '',
    },
    {
        name: 'Проблема №3',
        short: [
            'Недостаточное владение важного для Вас материала «Оценка результатов моделирования» (u30) порождают сложность в освоении следующей (не менее важной) связанной дидактической единицы «Расчёт погрешностей» (u31), также влияющих на освоение указанных выше компетенций УК-2, УК-6 и особенно ОПК-2 (выделена Вами как ключевая).  ',
        ],
    },
]

const rec = [
    'В учебной теме t1 рекомендуется повторно проработать материал «Теория принятия решений (ТПР). Терминология» (u2) и «Задача ТПР. Альтернативы и их классификация» (u3); в теме t2 следует повторить материал «Групповой (экспертный) выбор» (u7); а в теме t3 обратиться к дидактической единице «Метод Монте-Карло» (u15). Это позволит быстрее освоить материал дисциплины, имеющей важное значения для вашего профессионального роста, а также улучшить показатели по дисциплинам «Интеллектуальный анализ данных» (d1), «Программная реализация математических моделей» (d2), «Системы искусственного интеллекта» (d6).',
]

const current1 = [
    'Ваши результаты обучения имеют положительную динамику (балл контрольного среза составил 55.8 и увеличился на 24 % по сравнению с предыдущим этапом контроля), но существенные проблемы возникает из-за недостаточного освоения отельных элементов ранее изученного учебного материала. Интенсивность процесса обучения оценивается как высокая, а стремление достичь заявленные цели обучения как средняя. Рекомендуем в первую очередь повторить материал “Теория принятия решений (ТПР). Терминология” (u2), «Групповой (экспертный) выбор» (u7) и «Имитация поведения дискретного процесса» (u15).',
]
function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role='tabpanel'
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}>
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    }
}

const ExpandMore = styled((props) => {
    const { expand, ...other } = props
    return <IconButton {...other} />
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}))

export default function () {
    const [current, setCurrent] = React.useState(current1)
    const [expanded, setExpanded] = React.useState(false)
    const handleExpandClick = () => {
        setExpanded(!expanded)
    }
    const [value, setValue] = React.useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const [scaleMultiplier, setScaleMultiplier] = React.useState(1)
    const [drawingMode, setDrawingMode] = React.useState('default')
    const [actionItem, setActionItem] = React.useState()
    const [clickedInfo, setClickedInfo] = React.useState()
    const [labels, setLabels] = React.useState()
    const [redFlags, setRedFlags] = React.useState()
    const [scaleButtonUp, setScaleButtonUp] = React.useState({
        isActive: false,
    })
    const [scaleButtonDown, setScaleButtonDown] = React.useState({
        isActive: false,
    })
    const [scaleLoad, setScaleLoad] = React.useState(null)
    const [tree, setTree] = React.useState([])
    const [compTree, setCompTree] = React.useState([])
    const [upperInfo, setUpperInfo] = React.useState({
        aspectName: null,
        currentLoadingName: null,
        scaleType: null,
    })
    const [drawInfo, setDrawInfo] = React.useState(null)
    const [itemsText, setItemsText] = React.useState([])
    const [download, setDownload] = React.useState(false)
    const [oneLevel, setOneLevel] = React.useState(false)
    const [clickTrajectory, setClickTrajectory] = React.useState([])
    const [drawExternal, setDrawExternal] = React.useState(false)

    return (
        <Paper>
            <Grid container>
                <Grid item xs={2}>
                    <TestCheck
                        setLabels={setLabels}
                        setTree={setTree}
                        setDrawInfo={setDrawInfo}
                        setRedFlags={setRedFlags}
                        setUpperInfo={setUpperInfo}
                    />
                </Grid>
                <Grid item xs={8}>
                    <Grid item xs={12}>
                        <Typography
                            variant='h6'
                            sx={{
                                marginTop: 2,
                                paddingRight: 4,
                                marginLeft: 2,
                            }}>
                            Общая оценка ситуации
                        </Typography>
                        <Typography
                            sx={{
                                marginBottom: 2,

                                paddingRight: 4,
                                marginLeft: 2,
                            }}>
                            {current}
                        </Typography>
                        <Typography
                            variant='h6'
                            sx={{
                                marginTop: 2,
                                paddingRight: 4,
                                marginLeft: 2,
                            }}>
                            Основные проблемы
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Grid item xs={8}>
                            <Box
                                sx={{
                                    height: '350px',
                                    width: '100%',

                                    paddingRight: 4,
                                    marginLeft: 2,
                                    bgcolor: 'background.paper',
                                    overflow: 'auto',
                                }}>
                                <List
                                    component='nav'
                                    aria-label='secondary mailbox folder'>
                                    <ListItemButton selected>
                                        <ListItemText
                                            primary={recomendations[0].name}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        sx={{
                                                            display: 'inline',
                                                        }}
                                                        component='span'
                                                        variant='body2'
                                                        color='text.primary'>
                                                        {
                                                            recomendations[0]
                                                                .short
                                                        }
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItemButton>
                                    <ListItemButton>
                                        <ListItemText
                                            primary={recomendations[1].name}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        sx={{
                                                            display: 'inline',
                                                        }}
                                                        component='span'
                                                        variant='body2'
                                                        color='text.primary'>
                                                        {
                                                            recomendations[1]
                                                                .short
                                                        }
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItemButton>
                                    <ListItemButton>
                                        <ListItemText
                                            primary={recomendations[2].name}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        sx={{
                                                            display: 'inline',
                                                        }}
                                                        component='span'
                                                        variant='body2'
                                                        color='text.primary'>
                                                        {
                                                            recomendations[2]
                                                                .short
                                                        }
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItemButton>
                                </List>
                            </Box>
                        </Grid>
                        <Grid item xs={4} sx={{ textAlign: 'center' }}>
                            {labels ? (
                                <DrawCanvas
                                    scaleMultiplier={0.5}
                                    drawingMode={'light'}
                                    labels={labels}
                                    setActionItem={setActionItem}
                                    setClickedInfo={setClickedInfo}
                                    setDownload={setDownload}
                                    download={download}
                                    redFlags={redFlags}
                                    oneLevel
                                    dialogSized
                                />
                            ) : null}
                        </Grid>
                    </Grid>

                    {/** <Grid item xs={4} sx={{ textAlign: 'center' }}>
                    <img src={map} width="310px" height="300px" />
                </Grid>*/}
                    <Grid item xs={12} sx={{ m: 2 }}>
                        <Paper elevation={3}>
                            <Typography sx={{ m: 2 }} variant='h6'>
                                Рекомендации к действию
                            </Typography>
                            <Typography sx={{ m: 2 }} variant='body2'>
                                {rec}
                            </Typography>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                        <Paper elevation={3} sx={{ ml: 2 }}>
                            <Grid container>
                                <Grid item xs={6}>
                                    <Typography
                                        variant='h6'
                                        component='div'
                                        sx={{ ml: 2 }}>
                                        Задать вопрос обучающей системе
                                    </Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <ExpandMore
                                        expand={expanded}
                                        onClick={handleExpandClick}
                                        aria-expanded={expanded}
                                        aria-label='show more'>
                                        <ExpandCircleDownIcon
                                            color='primary'
                                            sx={{
                                                textAlign: 'left',
                                                fontSize: 40,
                                            }}
                                        />
                                    </ExpandMore>
                                </Grid>
                            </Grid>

                            <Collapse in={expanded} unmountOnExit>
                                <Grid container>
                                    <Grid item xs={3}>
                                        <StockSelect
                                            label={'Kind of the question'}
                                            value={'Why'}
                                        />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <StockSelect
                                            label={'Subject'}
                                            value={'Learning material fragment'}
                                        />
                                    </Grid>
                                    <Grid item xs={5}>
                                        <StockSelect
                                            label={'Value'}
                                            value={'u_7 - Monte-Carlo method'}
                                        />
                                    </Grid>
                                    <Grid item xs={3}>
                                        <StockSelect
                                            label={'Process of interest'}
                                            value={'Assessed / defined '}
                                        />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <StockSelect
                                            label={'Parameter'}
                                            value={'Subject assessment'}
                                        />
                                    </Grid>
                                    <Grid item xs={5}>
                                        <StockSelect
                                            label={'Value'}
                                            value={'Not successful enough'}
                                        />
                                    </Grid>
                                    <Grid item xs={7}>
                                        <FormGroup sx={{ m: 2 }}>
                                            <FormControlLabel
                                                control={<Checkbox checked />}
                                                label="Visualize values for entities that aren't mentioned in the selected recommendation"
                                            />
                                            <FormControlLabel
                                                control={<Checkbox />}
                                                label='Visualize external links to other types of entities on the map'
                                            />
                                            <FormControlLabel
                                                control={<Checkbox />}
                                                label='Go beyond the current scope of analysis for the explanation'
                                            />
                                        </FormGroup>
                                    </Grid>

                                    <Grid item xs={5}>
                                        <Button
                                            variant='contained'
                                            fullWidth
                                            sx={{
                                                marginTop: 5,
                                                marginBottom: 3,
                                                width: '97%',
                                            }}>
                                            Perform request
                                        </Button>
                                        <Button
                                            variant='contained'
                                            fullWidth
                                            sx={{ width: '97%' }}>
                                            Help
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Collapse>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    )
}

function StockSelect({ label, value }) {
    return (
        <FormControl
            sx={{
                width: '100%',
                marginBottom: 2,
                marginTop: 2,
                paddingRight: 4,
                marginLeft: 2,
            }}>
            <InputLabel id='demo-simple-select-label'>{label}</InputLabel>
            <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                value={1}
                label={label}>
                <MenuItem value={1}>{value}</MenuItem>
            </Select>
        </FormControl>
    )
}
