import React from 'react'
import DrawCanvas from '../../draw/DrawCanvas'
import BaseUI from '../../ui/BaseUI'
import {Paper, Grid, Typography, Collapse, Button, Box} from '@mui/material'
import Additional from './components/Additional'
import drawOneLevelCMKD from '../../draw/drawOneLevelCMKD'
import IconButton from '@mui/material/IconButton'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward'
import axios from './/..//..//utils/axios'
export default function App() {
    const [scaleMultiplier, setScaleMultiplier] = React.useState(1)
    const [drawingMode, setDrawingMode] = React.useState('default')
    const [actionItem, setActionItem] = React.useState()
    const [clickedInfo, setClickedInfo] = React.useState()
    const [labels, setLabels] = React.useState()
    const [redFlags, setRedFlags] = React.useState()
    const [scaleButtonUp, setScaleButtonUp] = React.useState({
        isActive: false,
    })
    const [scaleButtonDown, setScaleButtonDown] = React.useState({
        isActive: false,
    })
    const [scaleLoad, setScaleLoad] = React.useState(null)
    const [tree, setTree] = React.useState([])
    const [compTree, setCompTree] = React.useState([])
    const [upperInfo, setUpperInfo] = React.useState({
        aspectName: null,
        currentLoadingName: null,
        scaleType: null,
    })
    const [position, setPosition] = React.useState(999)
    const [expanded, setExpanded] = React.useState(false)
    const [drawInfo, setDrawInfo] = React.useState(null)
    const [itemsText, setItemsText] = React.useState([])
    const [download, setDownload] = React.useState(false)
    const [oneLevel, setOneLevel] = React.useState(false)
    const [clickTrajectory, setClickTrajectory] = React.useState([])
    const [drawExternal, setDrawExternal] = React.useState(false)
    const [showSupport, setShowSupport] = React.useState(false)
    const [showImportant, setShowImportant] = React.useState(false)
    //console.log(clickTrajectory)
    //-------

    React.useEffect(() => {
        if (scaleButtonUp.isActive) {
            axios
                .get(`/api/edelement/${scaleButtonUp.object.parent_id}`, {})
                .then(({data}) => {
                    setScaleLoad({
                        id: data.parent_id,
                        lowestLevel: data.level + 1,
                    })
                })
        }
    }, [scaleButtonUp])

    React.useEffect(() => {
        if (scaleButtonDown.isActive) {
        }
    }, [scaleButtonDown])

    React.useEffect(() => {
        if (labels) {
            if (drawExternal) {
                console.log(drawExternal)
            } else {
                console.log(drawExternal)
            }
        }
    }, [drawExternal])

    if (scaleMultiplier > 0)
        return (
            <Paper>
                <Grid container>
                    {upperInfo.currentLoadingName != null && (
                        <>
                            <Grid item xs={3}></Grid>
                            <Grid item xs={6}>
                                <Typography
                                    variant='h5'
                                    sx={{
                                        textAlign: 'center',
                                    }}>
                                    Карта для {upperInfo.currentLoadingName}
                                </Typography>
                                <Typography
                                    variant='h5'
                                    sx={{
                                        textAlign: 'center',
                                    }}>
                                    Масштаб: {upperInfo.scaleType}
                                </Typography>
                                <Typography
                                    variant='h5'
                                    sx={{
                                        textAlign: 'center',
                                    }}>
                                    Аспект: {upperInfo.aspectName}
                                </Typography>
                            </Grid>
                            <Grid item xs={3}></Grid>
                        </>
                    )}

                    <Grid item xs={3}>
                        <BaseUI
                            setScaleMultiplier={setScaleMultiplier}
                            setDrawingMode={setDrawingMode}
                            setLabels={setLabels}
                            setTree={setTree}
                            setCompTree={setCompTree}
                            setUpperInfo={setUpperInfo}
                            setDownload={setDownload}
                            setDrawInfo={setDrawInfo}
                            drawInfo={drawInfo}
                            setRedFlags={setRedFlags}
                            setOneLevel={setOneLevel}
                            drawExternal={drawExternal}
                            setDrawExternal={setDrawExternal}
                            position={position}
                            setPosition={setPosition}
                            showSupport={showSupport}
                            setShowSupport={setShowSupport}
                            showImportant={showImportant}
                            setShowImportant={setShowImportant}
                        />
                    </Grid>

                    {labels ? (
                        <>
                            <Grid item xs={6}>
                                <DrawCanvas
                                    scaleMultiplier={scaleMultiplier}
                                    drawingMode={drawingMode}
                                    labels={labels}
                                    setActionItem={setActionItem}
                                    setClickedInfo={setClickedInfo}
                                    setDownload={setDownload}
                                    download={download}
                                    redFlags={redFlags}
                                    oneLevel={oneLevel}
                                    position={position}
                                    extShowSupportRect={showSupport}
                                    showImportant={showImportant}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <Typography
                                    variant='h5'
                                    sx={{
                                        textAlign: 'center',
                                        m: 2,
                                    }}>
                                    Дополнительная информация
                                </Typography>

                                {scaleButtonDown.isActive && (
                                    <IconButton
                                        aria-label='settings'
                                        onClick={(event) => {
                                            setClickedInfo(null)
                                            drawOneLevelCMKD(
                                                setLabels,
                                                setTree,
                                                scaleButtonDown.object.id, //87,
                                                drawInfo.competence_id,
                                                scaleButtonDown.lowestLevel, //2
                                                drawInfo.user_id,
                                                drawInfo.date,
                                                drawInfo.plan_id,
                                                setRedFlags,
                                                setUpperInfo,
                                                drawInfo.drawExternal,
                                                null,
                                                drawInfo.position
                                            )
                                            setOneLevel(true)
                                            setDrawInfo((prev) => ({
                                                ...prev,
                                                currentLoad_id:
                                                    scaleButtonDown.object.id,
                                                lowestLevel:
                                                    scaleButtonDown.lowestLevel,
                                            }))
                                        }}>
                                        <ArrowDownwardIcon
                                            sx={{fontSize: 40}}
                                        />
                                    </IconButton>
                                )}

                                {scaleButtonUp.isActive &&
                                    scaleLoad != null && (
                                        <IconButton
                                            aria-label='settings'
                                            onClick={(event) => {
                                                setClickedInfo(null)
                                                drawOneLevelCMKD(
                                                    setLabels,
                                                    setTree,
                                                    scaleLoad.id, //87,
                                                    drawInfo.competence_id,
                                                    scaleLoad.lowestLevel, //2
                                                    drawInfo.user_id,
                                                    drawInfo.date,
                                                    drawInfo.plan_id,
                                                    setRedFlags,

                                                    setUpperInfo,
                                                    drawInfo.drawExternal,
                                                    null,
                                                    drawInfo.position
                                                )
                                                setOneLevel(true)
                                                setDrawInfo((prev) => ({
                                                    ...prev,
                                                    currentLoad_id:
                                                        scaleLoad.id,
                                                    lowestLevel:
                                                        scaleLoad.lowestLevel,
                                                }))
                                            }}>
                                            <ArrowUpwardIcon
                                                sx={{fontSize: 40}}
                                            />
                                        </IconButton>
                                    )}

                                <Additional
                                    actionItem={actionItem}
                                    clickedInfo={clickedInfo}
                                    tree={tree}
                                    setScaleButtonUp={setScaleButtonUp}
                                    setScaleButtonDown={setScaleButtonDown}
                                />
                            </Grid>
                        </>
                    ) : null}
                </Grid>
            </Paper>
        )
}
