import React from 'react'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
import { TextField, Typography, Grid } from '@mui/material'
import TestRadioGroup from '../../../components/TestRadioGroup'
import CheckboxList from '../../../components/CheckboxList'
const quests = [
    {
        name: 'Понятность и достаточность блока 1 в тексте пояснений',
        type: 'block1',
        num: 1,
    },
    {
        name: 'Понятность и достаточность блока 2 в тексте пояснений',
        type: 'block2',
        num: 2,
    },
    {
        name: 'Понятность и достаточность блока 3 в тексте пояснений',
        type: 'block3',
        num: 3,
    },
    {
        name: 'Согласие с аргументацией',
        type: 'agree',
        num: 4,
    },
    {
        name: 'Насколько карта понятно отражает ситуацию',
        type: 'cmkd',
        num: 5,
    },
]

export default function ({
    setRG,
    rG,
    textDE,
    setTextDE,
    textArgs,
    setTextArgs,
    setList,
    list,
    idText,
}) {
    let radiogroups = []
    quests.map(elem => {
        radiogroups.push(
            <>
                {(elem.type == 'block1' ||
                    elem.type == 'block3' ||
                    elem.type == 'cmkd') && <Grid item xs={3}></Grid>}
                <Grid item xs={3}>
                    {((idText <= 3 && elem.type != 'cmkd') || idText > 3) && (
                        <TestRadioGroup
                            type={elem.type}
                            name={elem.name}
                            setRG={setRG}
                            rG={rG}
                        />
                    )}
                </Grid>
                {(elem.type == 'block2' || elem.type == 'agree') && (
                    <Grid item xs={3}></Grid>
                )}
            </>,
        )
    })
    return (
        <>
            <Grid item xs={12}></Grid>

            {radiogroups}
            <Grid item xs={6}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Typography sx={{ mb: 2 }}>
                    К какой из дидактических единиц после такой рекомендации вы
                    предпочтете обратиться в первую очередь (напишите шифр
                    материала, например, u2)?{' '}
                </Typography>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <TextField
                    fullWidth
                    label="ответ"
                    sx={{ mb: 2 }}
                    value={textDE}
                    onChange={event => {
                        setTextDE(event.target.value)
                    }}
                />
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Typography sx={{ mb: 2 }}>
                    Какой из аргументов для Вас при этом был наиболее
                    убедителен?
                </Typography>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <TextField
                    fullWidth
                    label="ответ"
                    sx={{ mb: 2 }}
                    value={textArgs}
                    onChange={event => {
                        setTextArgs(event.target.value)
                    }}
                />
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Typography sx={{ mb: 2 }}>
                    Чего Вам не хватало для понимания учебной ситуации
                </Typography>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <CheckboxList list={list} setList={setList} />
            </Grid>
            <Grid item xs={3}></Grid>
        </>
    )
}
