import { useState, useEffect } from 'react'
import { Typography } from '@mui/material'
export default function ({
    actionItem,
    clickedInfo,
    tree,
    setScaleButtonUp,
    setScaleButtonDown,
}) {
    const [text, setText] = useState()
    const [retext, setRetext] = useState(false)

    useEffect(() => {
        if (clickedInfo) {
            if (clickedInfo.type === 'label') {
                let t = []
                let color = 'black'
                if (clickedInfo.objLabel.score < 0) color = 'red'
                t.push(
                    <Typography sx={{ color: color, maxWidth: 350 }}>
                        Объект: {clickedInfo.objLabel.object.name}
                    </Typography>,
                    clickedInfo.objLabel.percent !== -1 ? (
                        <Typography sx={{ maxWidth: 350 }}>
                            Пройден на {clickedInfo.objLabel.percent} процентов
                        </Typography>
                    ) : null,
                )

                if (clickedInfo.prevLabels.length !== 0) {
                    t.push(<Typography>Он опирается на: </Typography>)
                    clickedInfo.prevLabels.map(label => {
                        let color = 'black'
                        if (label.score < 0) color = 'red'
                        t.push(
                            <Typography
                                sx={{
                                    color: color,
                                    wordBreak: 'break-word',
                                    maxWidth: 350,
                                }}
                            >
                                - {label.object.name}
                            </Typography>,
                        )
                    })
                }
                if (clickedInfo.nextLabels.length !== 0) {
                    t.push(<Typography>Он влияет на: </Typography>)
                    clickedInfo.nextLabels.map(label => {
                        let color = 'black'
                        if (label.score < 0) color = 'red'
                        t.push(
                            <Typography
                                sx={{
                                    color: color,
                                    wordBreak: 'break-word',
                                    maxWidth: 350,
                                }}
                            >
                                - {label.object.name}
                            </Typography>,
                        )
                    })
                }

                setText(t)
                setScaleButtonUp({
                    isActive: false,
                })
                setScaleButtonDown({
                    isActive: false,
                })
            }
            if (clickedInfo.type === 'line') {
                let t = []
                let color = 'black'
                if (
                    clickedInfo.objLabelIn.score < 0 ||
                    clickedInfo.objLabelOut.score < 0
                )
                    color = 'red'
                t.push(
                    <Typography
                        sx={{
                            color: color,
                            wordBreak: 'break-word',
                            maxWidth: 350,
                        }}
                    >
                        Объект "{clickedInfo.objLabelIn.object.name}" влияет на
                        объект "{clickedInfo.objLabelOut.object.name}"
                    </Typography>,
                )
                setText(t)
                setScaleButtonUp({
                    isActive: false,
                })
                setScaleButtonDown({
                    isActive: false,
                })
            }
            if (clickedInfo.type === 'sector') {
                let t = []
                let color = 'black'
                let object = tree.find(({ id }) => id == clickedInfo.id)
                t.push(
                    <Typography
                        sx={{
                            color: color,
                            wordBreak: 'break-word',
                            maxWidth: 350,
                        }}
                    >
                        {tree.find(({ id }) => id == clickedInfo.id).name}
                    </Typography>,
                )
                setText(t)
                if (object.edelementtype_id < 4)
                    setScaleButtonDown({
                        isActive: true,
                        object: object,
                        lowestLevel: object.level + 2,
                    })
                if (object.edelementtype_id > 2)
                    setScaleButtonUp({
                        isActive: true,
                        object: object,
                    })
            }
        } else {
            setText('')
            setScaleButtonUp({
                isActive: false,
            })
            setScaleButtonDown({
                isActive: false,
            })
        }
    }, [clickedInfo, retext])

    useEffect(() => {
        let oldT = text
        if (actionItem) {
            if (actionItem.type === 'LabelOnMouseOver') {
                setText(
                    <Typography sx={{ wordBreak: 'break-word', maxWidth: 350 }}>
                        Объект {actionItem.objLabel.object.name}
                    </Typography>,
                )
            }
            if (actionItem.type === 'LineOnMouseOver') {
                setText(
                    <Typography sx={{ wordBreak: 'break-word', maxWidth: 350 }}>
                        Объект "{actionItem.objLabelIn.object.name}" влияет на
                        объект "{actionItem.objLabelOut.object.name}"
                    </Typography>,
                )
            }
        } else if (!clickedInfo) {
            setText('')
        } else setRetext(!retext)
    }, [actionItem])
    return <>{text}</>
}
