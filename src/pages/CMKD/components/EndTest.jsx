import React from 'react'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
import { TextField, Typography, Grid } from '@mui/material'
import TestRadioGroup from '../../../components/TestRadioGroup'

const quests = [
    {
        name: 'Насколько карты в нотации ККДЗ перегружены деталями (нужно упрощение)',
        type: 'detail',
        num: 1,
    },
    {
        name: 'Для оценки учебной ситуации насколько помогает наличие карты',
        type: 'score',
        num: 2,
    },
    {
        name: 'Функция интерактивности (смены аспектов) насколько была полезна для анализа учебной ситуации',
        type: 'interact',
        num: 3,
    },
    {
        name: 'Как Вы считаете, помогла бы сводная* карта для анализа учебной ситуации',
        type: 'summary',
        num: 4,
    },
]

export default function ({ setRG, rG, text, setText }) {
    let radiogroups = []
    quests.map(elem => {
        radiogroups.push(
            <>
                {(elem.type == 'detail' || elem.type == 'interact') && (
                    <Grid item xs={3}></Grid>
                )}
                <Grid item xs={3}>
                    <TestRadioGroup
                        type={elem.type}
                        name={elem.name}
                        setRG={setRG}
                        rG={rG}
                    />
                </Grid>
                {(elem.type == 'score' || elem.type == 'summary') && (
                    <Grid item xs={3}></Grid>
                )}
            </>,
        )
    })
    return (
        <>
            <Grid item xs={12}></Grid>
            {radiogroups}
            <Grid item xs={12}></Grid>

            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Typography sx={{ mb: 2 }}>
                    * Сводная карта – это карта, в которой на одном пространстве
                    дидактического материала одновременно сведены проблемные
                    моменты относительно всех аспектов анализа сразу: знаний,
                    личностных целей, компетенций.
                </Typography>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={12}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Typography sx={{ mb: 2 }}>
                    Если обозначить рекомендательный текст без карты через 1,
                    статичную частную карту через 2, динамическую частную карту
                    через 3, упрощенную карту через 4, то в какой
                    последовательности вы бы предпочли их использовать в задаче
                    анализа СВОЕЙ учебной ситуации (от самой «результативной» до
                    самой «нерезультативной» для анализа) в формате, например,
                    4-3-2-1:
                </Typography>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <TextField
                    fullWidth
                    label="ответ"
                    sx={{ mb: 2 }}
                    value={text}
                    onChange={event => {
                        setText(event.target.value)
                    }}
                />
            </Grid>
            <Grid item xs={3}></Grid>
        </>
    )
}
