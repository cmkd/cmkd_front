import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import {useState} from 'react'
import {FormHelperText, IconButton, InputAdornment} from '@mui/material'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import axios from '../../utils/axios'

export default function Login() {
    const [values, setValues] = useState({
        password: '',
        showPassword: false,
    })
    const handleClickShowPassword = () => {
        setValues({...values, showPassword: !values.showPassword})
    }

    const handleSubmit = async (event) => {
        async function auth(login, password) {
            await axios
                .post('/api/login', {
                    login: login,
                    password: password,
                })
                .then(({data}) => {
                    localStorage.setItem('_token', data._token)
                    window.location.replace('/lms')
                })
        }
        event.preventDefault()
        const data = new FormData(event.currentTarget)
        const login = data.get('login')
        const password = data.get('password')
        auth(login, password)
    }

    return (
        <Box
            sx={{
                marginTop: 0,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}>
            <Box
                component='form'
                onSubmit={handleSubmit}
                noValidate
                sx={{
                    mt: 1,
                    width: '100%',
                }}>
                <TextField
                    margin='normal'
                    // required
                    fullWidth
                    id='login'
                    label='Логин'
                    name='login'
                    autoComplete='on'
                    autoFocus
                />
                <TextField
                    margin='normal'
                    fullWidth
                    name='password'
                    label='Пароль'
                    id='password'
                    autoComplete='on'
                    type={values.showPassword ? 'text' : 'password'}
                    value={values.password}
                    onChange={(e) =>
                        setValues({
                            ...values,
                            password: e.target.value,
                        })
                    }
                    InputProps={{
                        style: {
                            paddingRight: '0px',
                            paddingLeft: '0px',
                        },
                        endAdornment: (
                            <InputAdornment
                                position='end'
                                className='loginAdorment'
                                style={{
                                    margin: '5px',
                                }}>
                                <IconButton
                                    aria-label='toggle password visibility'
                                    onClick={handleClickShowPassword}
                                    edge='end'
                                    style={{
                                        marginRight: '0px',
                                    }}>
                                    {values.showPassword ? (
                                        <VisibilityIcon />
                                    ) : (
                                        <VisibilityOffIcon />
                                    )}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <Button
                    type='submit'
                    fullWidth
                    variant='contained'
                    sx={{
                        mt: 2,
                        mb: 2,
                        height: (theme) => theme.spacing(6),
                    }}>
                    Войти
                </Button>
            </Box>
        </Box>
    )
}
