import {Routes, Route, Navigate, Outlet} from 'react-router-dom'
import Lazy from '../components/Lazy'
/** Layouts */
const LayoutMain = Lazy(() => import('../layouts/Main'))

/**Страницы */
const Home = Lazy(() => import('../pages/Home'))
const UserCourse = Lazy(() => import('../pages/Course'))
const Test = Lazy(() => import('../pages/Course/Test'))
const ReadEdElement = Lazy(() => import('../pages/Course/EdElement'))
const CMKD = Lazy(() => import('../pages/CMKD'))
const Auth = Lazy(() => import('../pages/Auth'))
const Dialog = Lazy(() => import('../pages/CMKD/Dialog'))
const DB = Lazy(() => import('../pages/DB'))
const DBEdElement = Lazy(() => import('../pages/DB/EdElement'))
const DBMethod = Lazy(() => import('../pages/DB/Method'))
const DBTask = Lazy(() => import('../pages/DB/Task'))
const DBCompetence = Lazy(() => import('../pages/DB/Competence'))
const DBPersonal = Lazy(() => import('../pages/DB/PersonalPreference'))
const DBFiles = Lazy(() => import('../pages/DB/FileStorage'))
const DBProfile = Lazy(() => import('../pages/DB/Profile'))
const DBCourse = Lazy(() => import('../pages/DB/Course'))
const DBDFP = Lazy(() => import('../pages/DB/DFP'))
const PostTestDialog = Lazy(() => import('../pages/Course/PostTestDialog'))

export default function () {
    return (
        <Routes>
            <Route path='/lms' element={<LayoutMain />}>
                <Route path='/lms/auth' element={<Auth />} />
                <Route path='/lms' element={<Home />} />
                <Route path='/lms/course' element={<UserCourse />} />
                <Route path='/lms/test' element={<Test />} />
                <Route path='/lms/read' element={<ReadEdElement />} />
                <Route path='/lms/test/after' element={<PostTestDialog />} />
                <Route path='/lms/dialog' element={<Dialog />} />
                <Route path='/lms/db' element={<DB />} />
                <Route path='/lms/db/edelement' element={<DBEdElement />} />
                <Route path='/lms/db/method' element={<DBMethod />} />
                <Route path='/lms/db/task' element={<DBTask />} />
                <Route path='/lms/db/files' element={<DBFiles />} />
                <Route path='/lms/db/competence' element={<DBCompetence />} />
                <Route path='/lms/db/personal' element={<DBPersonal />} />
                <Route path='/lms/db/profiles' element={<DBProfile />} />
                <Route path='/lms/db/courses' element={<DBCourse />} />
                <Route path='/lms/db/dfp' element={<DBDFP />} />
            </Route>
            <Route path='/lms/cmkd' element={<CMKD />} />
        </Routes>
    )
}
