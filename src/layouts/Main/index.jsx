/**
 * Main layout with nav bar and side menu
 */
import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Navigate, Outlet } from 'react-router-dom'
import { useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import useMediaQuery from '@mui/material/useMediaQuery'

import AppBar from '../components/AppBar'
import Main from '../components/Main'

import { pageActions } from '../../store'

function MainLayout() {
    const dispatch = useDispatch()
    const pageState = useSelector((state) => state.page)

    const theme = useTheme()
    const matchUpLg = useMediaQuery(theme.breakpoints.up('lg'))

    useEffect(() => {
        dispatch(pageActions.toggleDrawer(matchUpLg))
    }, [matchUpLg])

    const itemClickHandler = () => {
        if (matchUpLg) return
        dispatch(pageActions.toggleDrawer(false))
    }

    const toggleDrawerHandler = () => {
        dispatch(pageActions.toggleDrawer())
    }

    return (
        <Box sx={{ display: 'flex' }}>
            <AppBar toggleDrawer={toggleDrawerHandler} />

            <Main>
                <Outlet />
            </Main>
        </Box>
    )
}

export default MainLayout
