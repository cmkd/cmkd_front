import { styled } from '@mui/material/styles'
import MuiContainer from '@mui/material/Container'
import Paper from '@mui/material/Paper'
const Container = styled(MuiContainer)(({ theme }) => ({
    flexGrow: 1,
    marginTop: theme.typography.appBarHeight,
    padding: theme.spacing(2),
}))

export default function ({ children }) {
    return (
        <Container maxWidth='lg'>
            <Paper sx={{ py: 1, px: 2, mt: 15 }}>{children}</Paper>
        </Container>
    )
}
