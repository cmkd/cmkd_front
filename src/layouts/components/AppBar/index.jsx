import * as React from 'react'
import {useNavigate} from 'react-router-dom'
import {styled} from '@mui/material/styles'
import MuiAppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import {useLocation} from 'react-router-dom'
import axios from '../../../utils/axios'
const AppBar = styled(MuiAppBar)(({theme}) => ({
    '& .MuiToolbar-root': {
        minHeight: theme.typography.appBarHeight,
    },
}))

export default function ({toggleDrawer}) {
    let location = useLocation()
    let navigate = useNavigate()

    const [consoleButton, setConsoleButton] = React.useState(false)

    React.useEffect(() => {
        if (
            localStorage.getItem('_token') == null &&
            window.location.pathname != '/lms/auth'
        )
            window.location.replace('/lms/auth')
        else if (localStorage.getItem('_token') != null)
            axios
                .get(`/api/auth?token=${localStorage.getItem('_token')}`, {})
                .then(({data}) => {
                    setConsoleButton(
                        data.permissions.some((item) => {
                            return item.id == 1 || item.id == 2 || item.id == 4
                        })
                    )
                })
    }, [])

    return (
        <AppBar>
            <Toolbar>
                <Typography
                    variant='h6'
                    component='a'
                    onClick={() => {
                        async function dfp() {
                            let page_from = '/'
                            if (location.pathname == '/read')
                                page_from =
                                    'edelement ' +
                                    location.state.edelement.shortname
                            else page_from = location.pathname
                            await axios.post(`/api/user/dfp`, {
                                usereducationprofile_id:
                                    location.state.usereducationprofile_id,
                                page_from: page_from,
                                page_to: 'home',
                                page_component: 'appbar',
                                actiondfp: 'click',
                                parameters: '',
                            })
                        }
                        if (location.state) {
                            dfp()
                        }
                        navigate(`/lms/`)
                    }}
                    sx={{
                        color: 'inherit',
                        letterSpacing: '.15rem',
                        textDecoration: 'none',
                    }}>
                    AESU LMS
                </Typography>
                {consoleButton && (
                    <Typography
                        component='a'
                        sx={{
                            ml: 5,
                            color: 'inherit',
                            letterSpacing: '.15rem',
                            textDecoration: 'none',
                        }}
                        onClick={() => {
                            navigate(`/lms/db`)
                        }}>
                        Консоль
                    </Typography>
                )}
                {consoleButton && (
                    <Typography
                        component='a'
                        sx={{
                            ml: 5,
                            color: 'inherit',
                            letterSpacing: '.15rem',
                            textDecoration: 'none',
                        }}
                        onClick={() => {
                            navigate(`/lms/cmkd`)
                        }}>
                        CMKD
                    </Typography>
                )}

                <Box sx={{flexGrow: 1}} />
                {localStorage.getItem('_token') != null && (
                    <Typography
                        component='a'
                        sx={{
                            ml: 5,
                            color: 'inherit',
                            letterSpacing: '.15rem',
                            textDecoration: 'none',
                        }}
                        onClick={() => {
                            async function revoke() {
                                await axios.delete(`/api/token/revoke`, {})
                            }
                            revoke()
                            setConsoleButton(false)
                            localStorage.clear()
                            navigate(`/lms/auth`)
                        }}>
                        Выход
                    </Typography>
                )}
            </Toolbar>
        </AppBar>
    )
}
