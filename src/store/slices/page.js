import { createSlice } from '@reduxjs/toolkit'

export const pageSlice = createSlice({
    name: 'page',
    initialState: {
        mode: 'light',
        drawerOpen: true,
        snack: {
            message: null,
            pack: [],
        },
    },
    reducers: {
        toggleMode(state) {
            state.mode = state.mode === 'light' ? 'dark' : 'light'
        },
        toggleDrawer(state, action) {
            if (action.payload !== undefined) {
                state.drawerOpen = Boolean(action.payload)
            } else {
                state.drawerOpen = !state.drawerOpen
            }
        },
        addSnack(state, action) {
            if (!action.payload?.type || !action.payload?.value) return

            let { message, pack } = state.snack
            const _message = {
                ...action.payload,
                key: Math.random().toFixed(3),
            }

            if (!message && pack.length === 0) {
                message = _message
            } else if (pack.length < 2 && message.value !== _message.value) {
                pack.push(_message)
            }

            state.snack = { message, pack }
        },
        nextSnack(state) {
            let { message, pack } = state.snack

            if (pack.length > 0) {
                message = pack.shift()
            } else {
                message = null
                pack = []
            }

            state.snack = { message, pack }
        },
    },
})

export const pageActions = pageSlice.actions
