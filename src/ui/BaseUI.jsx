import {useState, useEffect} from 'react'
import Stack from '@mui/material/Stack'
import Slider from '@mui/material/Slider'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import Button from '@mui/material/Button'
import EnhancedSelect from '../components/EnhancedSelect'
import axios from '../utils/axios'
import {Grid, TextField} from '@mui/material'
import drawOneLevelCMKD from '../draw/drawOneLevelCMKD'
import drawSummaryCMKD from '../draw/drawSummaryCMKD'
import {DateTimePicker} from '@mui/x-date-pickers/DateTimePicker'
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker'
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs'
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider'
import FormGroup from '@mui/material/FormGroup'
import Checkbox from '@mui/material/Checkbox'
import 'dayjs/locale/ru'

const pathList = [
    {
        name: 'Учебный план',
        type: 'plan',
        num: 1,
    },
    {
        name: 'Семестр',
        type: 'sem',
        num: 2,
    },
]

export default function ({
    setScaleMultiplier,
    setDrawingMode,
    setLabels,
    setTree,
    setCompTree,
    setUpperInfo,
    setDownload,
    setDrawInfo,
    drawInfo,
    setRedFlags,

    setOneLevel,
    drawExternal,
    setDrawExternal,
    position,
    setPosition,
    showSupport,
    setShowSupport,
    showImportant,
    setShowImportant,
}) {
    const [scale, setScale] = useState(1)
    const [mode, setMode] = useState('default')
    const [editedType, setEditedType] = useState('')

    const [edelement, setEdelement] = useState({
        plan: '',
        sem: '',
        disc: '',
        branch_id: 0,
        test_id: 0,
        discipline_id: 0,
    })
    const [edelementList, setEdelementList] = useState({
        plan: [],
        sem: [],
        disc: [],
    })
    const [users, setUsers] = useState([])
    const [currentUser, setCurrentUser] = useState('')
    const [competences, setCompetences] = useState([])
    const [competence, setCompetence] = useState({
        id: 0,
        name: 'Знания',
    })
    const handleSlide = (event, newValue) => {
        setScaleMultiplier(newValue)
        setScale(newValue)
    }

    const handleChange = (event) => {
        setMode(event.target.value)
        setDrawingMode(event.target.value)
    }

    const [start_at, setStart_at] = useState(null)

    const handleChangeStart_at = (newValue) => setStart_at(newValue)

    const handleDrawExternal = (event) => {
        setDrawExternal(event.target.checked)
    }

    const handleShowSupport = (event) => {
        setShowSupport(event.target.checked)
    }

    const handleShowImportant = (event) => {
        setShowImportant(event.target.checked)
    }

    useEffect(() => {
        //для создания нового соединения - загрузка учебных планов

        axios
            .post(`/api/edelements`, {
                parent_id: null,
            })
            .then(({data}) => {
                if (data.total !== 0) {
                    let sortedData = data.items.sort(
                        (x, y) => x.order - y.order
                    )
                    setEdelementList((prev) => ({
                        ...prev,
                        plan: sortedData,
                    }))
                }
            })
    }, [])

    useEffect(() => {
        setUpperInfo((prev) => ({
            ...prev,
            aspectName: competence.name,
        }))
        if (currentUser !== '' && start_at !== null && drawInfo !== null) {
            console.log(drawInfo)
            setOneLevel(true)
            setDrawInfo((prev) => ({
                ...prev,
                competence_id: competence.id,
            }))
            drawOneLevelCMKD(
                setLabels,
                setTree,
                drawInfo.currentLoad_id, //87,
                competence.id,
                drawInfo.lowestLevel, //2,
                drawInfo.user_id,
                drawInfo.date,
                drawInfo.plan_id,
                setRedFlags,
                setUpperInfo,
                drawExternal,
                null,
                position
            )
        }
    }, [competence])

    useEffect(() => {
        async function fetchCompetences() {
            await axios
                .post(`/api/competences`, {
                    pageCount: 1,
                    perPage: 1000,
                })
                .then(({data}) => {
                    let c = []
                    c.push({
                        id: 0,
                        name: 'Знания',
                    })
                    c.push({
                        id: -1,
                        name: 'Личные предпочтения',
                    })
                    data.items.forEach((element) => {
                        c.push(element)
                    })

                    setCompetences(c)
                })
        }
        async function fetchUsers() {
            if (edelement.branch_id !== 0)
                axios
                    .get(`/api/guest/branch/${edelement.branch_id}/users`)
                    .then(({data}) => {
                        let options = data.map((elem) => {
                            const userName = `${elem.surname} ${elem.name} ${elem.patronym}`
                            return {
                                id: elem.id,
                                name: `${userName}`,
                            }
                        })
                        setUsers(options)
                    })
        }
        fetchCompetences()
        fetchUsers()
    }, [edelement])

    useEffect(() => {
        //для создания нового соединения - загрузка остальных образ. элементов
        if (editedType !== 'sem') {
            let type
            let current = pathList.filter((obj) => {
                return obj.type === editedType
            })
            if (current[0] !== undefined) {
                let next = pathList.filter((obj) => {
                    return obj.num === current[0].num + 1
                })
                type = next[0].type
            }

            axios
                .post(`/api/edelements`, {
                    parent_id: edelement[editedType],
                })
                .then(({data}) => {
                    if (data.total !== 0) {
                        let sortedData = data.items.sort(
                            (x, y) => x.order - y.order
                        )
                        setEdelementList((prev) => ({
                            ...prev,
                            [type]: sortedData,
                        }))
                    }
                })
        }
    }, [editedType, edelement])

    let addConnectionSelects = []
    pathList.map((step) => {
        addConnectionSelects.push(
            <EnhancedSelect
                name={step.name}
                type={step.type}
                menuItems={edelementList[step.type]}
                setParentValue={setEdelement}
                defaultValue={edelement[step.type]}
                setType={setEditedType}
                cmkd
            />
        )
    })

    return (
        <Grid container minWidth={400}>
            <Grid item xs={12}>
                <Button
                    variant='contained'
                    sx={{mb: 2}}
                    //onClick={handleDownloaded}
                    onClick={() => {
                        setDownload(true)
                    }}
                    fullWidth>
                    Сохранить
                </Button>
            </Grid>
            <Grid item xs={12}>
                <FormLabel sx={{m: 2}}>Масштаб</FormLabel>
                <Stack
                    spacing={2}
                    direction='row'
                    sx={{m: 2}}
                    alignItems='center'>
                    <Slider
                        aria-label='Volume'
                        value={scale}
                        onChange={handleSlide}
                        min={0.5}
                        max={3}
                        step={0.1}
                    />
                </Stack>
            </Grid>
            <Grid item xs={12}>
                <FormControl sx={{m: 2}}>
                    <FormLabel>Режим работы</FormLabel>
                    <RadioGroup value={mode} onChange={handleChange}>
                        <FormControlLabel
                            value='default'
                            control={<Radio />}
                            label='Базовая'
                        />
                        <FormControlLabel
                            value='default+'
                            control={<Radio />}
                            label='Индивидуализированная'
                        />
                        <FormControlLabel
                            value='score'
                            control={<Radio />}
                            label='Частная'
                        />
                        <FormControlLabel
                            value='light'
                            control={<Radio />}
                            label='Упрощённая частная'
                        />
                    </RadioGroup>
                </FormControl>
            </Grid>
            <Grid item xs={12}>
                {addConnectionSelects}
                <EnhancedSelect
                    name={'Пользователи'}
                    menuItems={users}
                    setParentValue={setCurrentUser}
                    defaultValue={currentUser}
                />
                <LocalizationProvider
                    dateAdapter={AdapterDayjs}
                    adapterLocale={'ru'}>
                    <DateTimePicker
                        sx={{mb: 2}}
                        label='Период до'
                        value={start_at}
                        mask={'__.__.____'}
                        //views={['month', 'year']}
                        onChange={handleChangeStart_at}
                        renderInput={(params) => (
                            <TextField {...params} fullWidth />
                        )}
                    />
                </LocalizationProvider>
                <EnhancedSelect
                    name={'Аспект'}
                    menuItems={competences}
                    setParentValue={setCompetence}
                    defaultValue={competence.id}
                    type={'competence'}
                />
                <TextField
                    key='position'
                    fullWidth
                    label={'Позиция последнего изученного элемента'}
                    value={position}
                    onChange={(event) => {
                        setPosition(event.target.value)
                    }}
                    sx={{mb: 2}}
                    type='number'
                />
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={drawExternal}
                                onChange={handleDrawExternal}
                            />
                        }
                        label='Показывать внешние связи ДЕ'
                    />
                </FormGroup>

                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={showSupport}
                                onChange={handleShowSupport}
                            />
                        }
                        label='Элементы вне карты'
                    />
                </FormGroup>

                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={showImportant}
                                onChange={handleShowImportant}
                            />
                        }
                        label='Выделить важные связи'
                    />
                </FormGroup>

                {currentUser !== '' && start_at !== null && (
                    <>
                        <Button
                            variant='contained'
                            sx={{mb: 2}}
                            //onClick={handleDownloaded}
                            onClick={() => {
                                setOneLevel(true)
                                setDrawInfo((prev) => ({
                                    ...prev,
                                    competence_id: competence.id,
                                    user_id: currentUser,
                                    date: start_at.$d,
                                    semester_id: edelement.sem,
                                    plan_id: edelement.plan,
                                    currentLoad_id: edelement.sem,
                                    lowestLevel: 3,
                                    drawExternal: drawExternal,
                                    position: position,
                                }))
                                drawOneLevelCMKD(
                                    setLabels,
                                    setTree,
                                    edelement.sem, //87,
                                    competence.id,
                                    3, //2,
                                    currentUser,
                                    start_at.$d,
                                    edelement.plan,
                                    setRedFlags,
                                    setUpperInfo,
                                    drawExternal,
                                    null,
                                    position
                                )
                            }}
                            fullWidth>
                            Нарисовать карту
                        </Button>
                        <Button
                            variant='contained'
                            sx={{mb: 2}}
                            //onClick={handleDownloaded}
                            onClick={() => {
                                setOneLevel(false)
                                drawSummaryCMKD(
                                    setLabels,
                                    setTree,
                                    edelement.sem,
                                    edelement.plan,
                                    setCompTree,
                                    currentUser,
                                    start_at.$d,
                                    setRedFlags
                                )
                            }}
                            fullWidth>
                            Нарисовать сводную карту
                        </Button>
                    </>
                )}
            </Grid>
        </Grid>
    )
}
