import { useState, useEffect } from 'react'
import Button from '@mui/material/Button'
import EnhancedSelect from '../components/EnhancedSelect'
import axios from '../utils/axios'
import drawOneLevelCMKD from '../draw/drawOneLevelCMKD'

export default function ({
    setLabels,
    setTree,
    basket_id,
    load_id,
    discipline_id,
    competence_id,
    setClicked,
    clicked,
}) {
    const [competences, setCompetences] = useState([
        {
            id: 0,
            name: 'Знаниевый аспект рассмотрения',
        },
        {
            id: 11,
            name: 'Демонстрировать владение системным подходом (анализ и синтез)',
        },
        {
            id: 12,
            name: 'Искать, обобщать, концентрировать и структурировать информацию, выделять существенное, делать выводы',
        },
        {
            id: 15,
            name: 'Владеть передовыми методами, подходами и инструментами, включая информационные технологи',
        },
    ])
    const [competence, setCompetence] = useState(competence_id)

    // useEffect(() => {
    //     async function fetchCompetences() {
    //         await axios
    //             .get(`/api/discipline/${load_id}/competences`, {})
    //             .then(({ data }) => {
    //                 let c = []
    //                 c.push({
    //                     id: 0,
    //                     name: 'нет',
    //                 })
    //                 data.forEach(element => {
    //                     c.push(element)
    //                 })
    //                 console.log(c)
    //                 setCompetences(c)
    //                 setCompetence(competence_id)
    //             })
    //     }
    //     fetchCompetences()
    // }, [load_id])
    return (
        <>
            <EnhancedSelect
                name={'Аспект'}
                menuItems={competences}
                setParentValue={setCompetence}
                defaultValue={competence}
            />
            <Button
                variant="contained"
                sx={{ mb: 2, maxWidth: 300 }}
                //onClick={handleDownloaded}
                onClick={() => {
                    drawOneLevelCMKD(
                        setLabels,
                        setTree,
                        basket_id,
                        discipline_id,
                        competence,
                    )
                    setClicked({
                        clicked: true,
                        competence: competence,
                    })
                }}
                fullWidth
            >
                Нарисовать карту
            </Button>
        </>
    )
}
