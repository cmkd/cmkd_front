/**
 * Components lazy loading
 */
import React, { Suspense } from 'react'
import { styled } from '@mui/material/styles'
import LinearProgress from '@mui/material/LinearProgress'

export const Progress = styled('div')({
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    zIndex: 1200,
})

export default function (method) {
    const Component = React.lazy(method)

    return props => (
        <Suspense
            fallback={
                <Progress>
                    <LinearProgress color="primary" />
                </Progress>
            }
        >
            <Component {...props} />
        </Suspense>
    )
}
