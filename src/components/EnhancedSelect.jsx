import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import * as React from 'react'
import Paper from '@mui/material/Paper'
export default function EnhancedSelect({
    name,
    type,
    menuItems,
    setParentValue,
    defaultValue,
    disabled,
    setType,
    quest,
    cmkd,
    mode,
    position,
}) {
    const [value, setValue] = React.useState(defaultValue)

    const handleChange = (event) => {
        const { value } = event.target

        setValue(value)
        if (setType !== undefined) setType(type)
        if (type === 'competence') {
            let competenceName = menuItems.find(
                ({ id }) => id == event.target.value
            ).name
            setParentValue({
                id: event.target.value,
                name: competenceName,
            })
        } else if (cmkd && type === 'plan') {
            let branch_id = menuItems.find(
                ({ id }) => id == event.target.value
            ).branch_id

            setParentValue((prev) => ({
                ...prev,
                [type]: event.target.value,
                branch_id: branch_id,
            }))
        } else if (cmkd && type === 'disc') {
            let discipline_id = menuItems.find(
                ({ id }) => id == event.target.value
            ).discipline_id

            setParentValue((prev) => ({
                ...prev,
                [type]: event.target.value,
                discipline_id: discipline_id,
            }))
        } else if (type !== undefined && type !== 'de')
            setParentValue((prev) => ({
                ...prev,
                [type]: event.target.value,
            }))
        else if (type === 'de') {
            let dePosition = menuItems.find(
                ({ id }) => id == event.target.value
            ).didacticdescription.packnumber

            setParentValue((prev) => ({
                ...prev,
                [type]: event.target.value,
                dePosition: dePosition,
            }))
        } else setParentValue(event.target.value)
    }

    const _menu = menuItems.map((elem) => {
        return (
            <MenuItem key={'list' + elem.id} value={elem.id} name={'www'}>
                {type === 'de'
                    ? elem.didacticdescription.packnumber + ' '
                    : null}
                {type == 'user_id' && elem.surname + ' '}
                {type && mode !== 'competences' && elem.name}
                {type == 'user_id' && ' ' + elem.patronym}
                {mode === 'competences' ? elem.code + ' ' + elem.name : null}
                {cmkd && type === undefined
                    ? elem.score +
                      ' ' +
                      elem.user.surname +
                      ' ' +
                      elem.user.name +
                      ' ' +
                      elem.passed_at +
                      ' ' +
                      elem.test.name
                    : null}
                {quest ? elem.pivot.position + ' ' + elem.name : null}
                {position ? elem.position + ' ' + elem.name : null}

                {!quest && !cmkd && !type && !mode && !position && elem.name}
            </MenuItem>
        )
    })

    return (
        <Paper sx={{ minWidth: 120, mb: 2 }}>
            <FormControl fullWidth disabled={disabled}>
                <InputLabel id='demo-simple-select-label'>{name}</InputLabel>
                <Select
                    labelId='demo-simple-select-label'
                    id='demo-simple-select'
                    value={value}
                    label={name}
                    onChange={handleChange}>
                    {_menu}
                </Select>
            </FormControl>
        </Paper>
    )
}
