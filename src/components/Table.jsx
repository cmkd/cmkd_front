import { useState, useEffect } from 'react'
import axios from '../utils/axios'
import SkeletonTable from './Skeleton/Table'
import {
    Tooltip,
    IconButton,
    Grid,
    Paper,
    TablePagination,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Dialog,
    DialogContent,
    Typography,
    Button,
    TextField,
} from '@mui/material'
import { Delete, Add, Edit } from '@mui/icons-material'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import EnhancedSelect from './EnhancedSelect'
import 'dayjs/locale/ru'
import dayjs from 'dayjs'

export default function ({
    type, //models (profiles) - работа с многими экземплярами
    oneItemType, //model (profile) - работа с одним экземпляром
    columnList, //{name: 'Колонка', type: 'column', dataType: 'string'} - типы полей для заполнения
    edelementLevel, //0-4, для добавления/редактирования сущностей, где некоторые поля - обр. элементы
}) {
    const [state, setState] = useState({
        page: 0,
        perPage: 15,
        items: [],
        total: 0,
        loading: true,
        completed: false,
    })

    const [clickedItem, setClickedItem] = useState()

    useEffect(() => {
        axios
            .post(`/api/${type}`, {
                perPage: state.perPage,
            })
            .then(({ data }) => {
                if (data.total !== 0) {
                    let items = data.items
                    items.forEach((item) => {
                        if (item.user != undefined)
                            item.fullname =
                                item.user.surname +
                                ' ' +
                                item.user.name +
                                ' ' +
                                item.user.patronym
                        if (item.edelement != undefined)
                            item.plan = item.edelement.shortname
                    })
                    setState((prev) => ({
                        ...prev,
                        page: state.page,
                        items: data.items,
                        total: data.total,
                        loading: false,
                        completed: data.lastPage,
                    }))
                }
            })
    }, [clickedItem])

    const handleChangePage = (_, newPage) => {
        if (
            state.loading ||
            newPage < 0 ||
            newPage * state.perPage >= state.total
        )
            return

        setState((prev) => ({ ...prev, page: newPage }))

        if (state.completed) return

        setState((prev) => ({ ...prev, loading: true }))

        axios
            .post(`/api/${type}`, {
                perPage: state.perPage,
                pageCount: newPage + 1,
            })
            .then(({ data }) => {
                let items = data.items
                items.forEach((item) => {
                    if (
                        item.user.name != undefined &&
                        item.user.surname != undefined &&
                        item.user.patronym != undefined
                    )
                        item.fullname =
                            item.user.surname +
                            ' ' +
                            item.user.name +
                            ' ' +
                            item.user.patronym
                    if (item.edelement != undefined)
                        item.plan = item.edelement.shortname
                })
                setState((prev) => ({
                    ...prev,
                    items: [...prev.items, ...data.items],
                    loading: false,
                    completed: data.lastPage,
                }))
            })
    }

    const handleChangeRowsPerPage = (event) => {
        const newPerPage = parseInt(event.target.value)

        setState((prev) => ({
            ...prev,
            perPage: newPerPage,
            loading: true,
        }))

        axios
            .post(`/api/${type}`, {
                perPage: newPerPage,
            })
            .then(({ data }) => {
                let items = data.items
                items.forEach((item) => {
                    if (
                        item.user.name != undefined &&
                        item.user.surname != undefined &&
                        item.user.patronym != undefined
                    )
                        item.fullname =
                            item.user.surname +
                            ' ' +
                            item.user.name +
                            ' ' +
                            item.user.patronym
                    if (item.edelement != undefined)
                        item.plan = item.edelement.shortname
                })
                setState((prev) => ({
                    ...prev,
                    items: data.items,
                    loading: false,
                    completed: data.lastPage,
                }))
            })
    }

    let headRow = []
    columnList.map((col) => {
        headRow.push(
            <TableCell
                key={col.type}
                sx={{ fontSize: 'h6.fontSize' }}
                width='auto'>
                {col.name}
            </TableCell>
        )
    })
    headRow.push(
        <TableCell key={'add'}>
            <Tooltip title='Создать'>
                <IconButton
                    aria-label='settings'
                    onClick={(event) => {
                        event.stopPropagation()
                        setClickedItem({
                            elem: null,
                            itemType: oneItemType,
                            toDo: 'add',
                        })
                    }}
                    disabled={clickedItem != undefined}>
                    <Add sx={{ fontSize: 40 }} color='primary' />
                </IconButton>
            </Tooltip>
        </TableCell>
    )

    const bodyRows = state.loading ? (
        <SkeletonTable rows={state.perPage} columns={columnList.length} />
    ) : (
        state.items
            .slice(
                state.page * state.perPage,
                state.page * state.perPage + state.perPage
            )
            .map((elem) => {
                let row = columnList.map((col) => (
                    <TableCell
                        key={col.type + elem.id}
                        sx={{ fontSize: 'h6.fontSize' }}>
                        {elem[col.type]}
                    </TableCell>
                ))
                row.push(
                    <TableCell key={'edit'}>
                        <Tooltip title='Отредактировать'>
                            <IconButton
                                aria-label='settings'
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setClickedItem({
                                        elem: elem,
                                        toDo: 'edit',
                                        itemType: oneItemType,
                                    })
                                }}
                                disabled={clickedItem != undefined}>
                                <Edit sx={{ fontSize: 40 }} />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title='Удалить'>
                            <IconButton
                                aria-label='settings'
                                onClick={(event) => {
                                    event.stopPropagation()
                                    setClickedItem({
                                        elem: elem,
                                        toDo: 'delete',
                                        itemType: oneItemType,
                                    })
                                }}
                                disabled={clickedItem != undefined}>
                                <Delete sx={{ fontSize: 40 }} />
                            </IconButton>
                        </Tooltip>
                    </TableCell>
                )

                return (
                    <TableRow
                        key={'pt-' + elem.id}
                        sx={{ '&:hover': { background: '#f1f1f1' } }}>
                        {row}
                    </TableRow>
                )
            })
    )
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 400 }}>
                        <TableHead>
                            <TableRow>{headRow}</TableRow>
                        </TableHead>
                        <TableBody>{bodyRows}</TableBody>
                    </Table>
                </TableContainer>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 15, 25]}
                    component='div'
                    count={state.total}
                    page={state.page}
                    rowsPerPage={state.perPage}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage='Строк:'
                    labelDisplayedRows={({ from, to, count }) =>
                        `${from} - ${to} из ${count}`
                    }
                />
            </Grid>
            <ClickedItemProcessing
                clickedItem={clickedItem}
                setClickedItem={setClickedItem}
                columnList={columnList}
                edelementLevel={edelementLevel}
            />
        </Grid>
    )
}

function ClickedItemProcessing({
    clickedItem,
    setClickedItem,
    edelementLevel,
    columnList,
}) {
    const handleClose = () => {
        setClickedItem()
    }
    if (clickedItem)
        return (
            <Dialog
                open={clickedItem !== undefined}
                onClose={handleClose}
                scroll={'paper'}
                maxWidth={'lg'}
                fullWidth
                aria-labelledby='scroll-dialog-title'
                aria-describedby='scroll-dialog-description'>
                <DialogContent>
                    {clickedItem.toDo == 'delete' && (
                        <DeleteItem
                            clickedItem={clickedItem}
                            setClickedItem={setClickedItem}
                        />
                    )}

                    {(clickedItem.toDo == 'edit' ||
                        clickedItem.toDo == 'add') && (
                        <AddEditItem
                            clickedItem={clickedItem}
                            setClickedItem={setClickedItem}
                            columnList={columnList}
                            edelementLevel={edelementLevel}
                        />
                    )}
                </DialogContent>
            </Dialog>
        )
}

function DeleteItem({ clickedItem, setClickedItem }) {
    return (
        <>
            <Grid item xs={12}>
                <Typography>
                    Подтвердите удаление {clickedItem.elem.name}
                </Typography>
            </Grid>
            <Button
                variant='contained'
                sx={{ margin: 2 }}
                onClick={() => {
                    axios
                        .delete(
                            `/api/${clickedItem.itemType}/${clickedItem.elem.id}`,
                            {}
                        )
                        .then(({ data }) => {
                            console.log(data)
                        })
                    setClickedItem()
                }}>
                Удалить
            </Button>
            <Button
                variant='contained'
                sx={{ margin: 2 }}
                onClick={() => {
                    setClickedItem()
                }}>
                Отменить
            </Button>
        </>
    )
}

function AddEditItem({
    clickedItem,
    setClickedItem,
    edelementLevel,
    columnList,
}) {
    const [params, setParams] = useState()
    const [components, setComponents] = useState()
    useEffect(() => {
        let _params = {}
        columnList.forEach((col) => {
            if (col.dataType == 'string') {
                //TextField
                _params[col.type] =
                    clickedItem.toDo == 'edit' ? clickedItem.elem[col.type] : ''
            } else if (col.dataType == 'date') {
                //DatePicker
                _params[col.type] =
                    clickedItem.toDo == 'edit'
                        ? clickedItem.elem[col.type]
                        : null
            } //EnhancedSelect
            else
                _params[col.dataType] =
                    clickedItem.toDo == 'edit'
                        ? clickedItem.elem[col.dataType]
                        : ''
        })
        setParams(_params)
    }, [])

    useEffect(() => {
        if (params) {
            let _components = []
            columnList.forEach((col) => {
                if (col.dataType == 'string') {
                    //TextField
                    _components.push(
                        <TextField
                            key={col.type}
                            fullWidth
                            label={col.name}
                            value={params[col.type]}
                            onChange={(event) => {
                                setParams((prev) => ({
                                    ...prev,
                                    [col.type]: event.target.value,
                                }))
                            }}
                            sx={{ mb: 2 }}
                        />
                    )
                } else if (col.dataType == 'date') {
                    //DatePicker
                    _components.push(
                        <Grid item xs={12}>
                            <LocalizationProvider
                                dateAdapter={AdapterDayjs}
                                adapterLocale={'ru'}>
                                <DesktopDatePicker
                                    sx={{ mb: 2 }}
                                    label={col.name}
                                    value={dayjs(params[col.type])}
                                    mask={'__.__.____'}
                                    onChange={(value) => {
                                        setParams((prev) => ({
                                            ...prev,
                                            [col.type]: value,
                                        }))
                                    }}
                                    renderInput={(params) => (
                                        <TextField {...params} fullWidth />
                                    )}
                                />
                            </LocalizationProvider>
                        </Grid>
                    )
                } //EnhancedSelect
                else {
                    _components.push(
                        <ForeignItemsSelect
                            params={params}
                            setParams={setParams}
                            edelementLevel={edelementLevel}
                            col={col}
                        />
                    )
                }
            })
            setComponents(_components)
        }
    }, [params])

    if (components)
        return (
            <>
                {components}
                <Button
                    variant='contained'
                    sx={{ margin: 2 }}
                    onClick={() => {
                        if (clickedItem.toDo == 'edit')
                            axios
                                .put(
                                    `/api/${clickedItem.itemType}/${clickedItem.elem.id}`,
                                    params
                                )
                                .then(({ data }) => {
                                    console.log(data)
                                })
                        else
                            axios
                                .post(`/api/${clickedItem.itemType}`, params)
                                .then(({ data }) => {
                                    console.log(data)
                                })
                        setClickedItem()
                    }}>
                    Сохранить
                </Button>
                <Button
                    variant='contained'
                    sx={{ margin: 2 }}
                    onClick={(event) => {
                        setClickedItem()
                    }}>
                    Отменить
                </Button>
            </>
        )
}

function ForeignItemsSelect({ params, setParams, edelementLevel, col }) {
    const [menuItems, setMenuItems] = useState()
    useEffect(() => {
        async function loadList() {
            await axios
                .post(`api/${col.dataType.split('_')[0] + 's'}/short`, {
                    level: edelementLevel,
                })
                .then(({ data }) => {
                    setMenuItems(data)
                })
        }
        loadList()
    }, [])

    if (menuItems)
        return (
            <EnhancedSelect
                name={col.name}
                type={col.dataType}
                menuItems={menuItems}
                setParentValue={setParams}
                defaultValue={params[col.dataType]}
            />
        )
}
