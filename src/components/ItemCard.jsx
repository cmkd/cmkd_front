import {Typography, Card, CardActionArea, CardContent} from '@mui/material'

export default function ({
    firstString,
    secondString,
    sx,
    setParentValue,
    cardID,
    cardType,
    objCard,
}) {
    const handleClick = () => {
        setParentValue({
            ID: cardID,
            type: cardType,
            objCard: objCard,
        })
    }

    return (
        <Card sx={sx}>
            <CardActionArea
                onClick={handleClick}
                disabled={cardType == 'passed'}>
                <CardContent>
                    <Typography gutterBottom variant='h6' component='div'>
                        {firstString}
                    </Typography>
                    <Typography variant='body2' color='text.secondary'>
                        {secondString}
                    </Typography>
                    {cardType == 'active' && objCard.edelement != null && (
                        <Typography variant='body2' color='text.secondary'>
                            Пройдено вплоть до "{objCard.edelement.name}"
                        </Typography>
                    )}
                    {cardType == 'active' && objCard.score > 0 && (
                        <Typography variant='body2' color='text.secondary'>
                            Для продолжения работы нажмите сюда
                        </Typography>
                    )}
                    {cardType == 'active' && objCard.score == 0 && (
                        <Typography variant='body2' color='text.secondary'>
                            Пройдите вступительное тестирование
                        </Typography>
                    )}
                </CardContent>
            </CardActionArea>
        </Card>
    )
}
