import * as React from 'react'
import { Grid, Button, TextField } from '@mui/material'
import axios from '../utils/axios'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'

export default function ({ type, types, discipline_id, setAddAction }) {
    const [state, setState] = React.useState({
        page: 0,
        perPage: 1000,
        items: [],
        total: 0,
        loading: true,
        completed: false,
    })
    const [name, setName] = React.useState('')
    const [elem, setElem] = React.useState(null)
    const [edit, setEdit] = React.useState(false)
    const [del, setDel] = React.useState(false)
    const [add, setAdd] = React.useState(false)
    const [reload, setReload] = React.useState(false)
    React.useEffect(() => {
        axios
            .post(`/api/${types}`, {
                perPage: state.perPage,
            })
            .then(({ data }) => {
                setState((prev) => ({
                    ...prev,
                    items: data.items,
                    total: data.total,
                    loading: false,
                    completed: data.lastPage,
                }))
            })
    }, [reload])

    React.useEffect(() => {
        if (elem !== null) {
            setName(elem.name)
        }
    }, [elem])

    const bodyRows = state.loading
        ? null
        : state.items
              .slice(
                  state.page * state.perPage,
                  state.page * state.perPage + state.perPage
              )
              .map((elem) => (
                  <ListItem disablePadding>
                      <ListItemButton
                          onClick={() => {
                              setElem(elem)
                              setAdd(false)
                          }}>
                          <ListItemText primary={elem.name} />
                      </ListItemButton>
                  </ListItem>
              ))

    return (
        <>
            <Grid item xs={2}>
                <Tooltip title='Добавить'>
                    <IconButton
                        aria-label='settings'
                        onClick={(event) => {
                            if (setAddAction) setAddAction(true)
                            else setAdd(true)
                            setEdit(false)
                            setDel(false)
                            setElem(null)
                            setName(null)
                        }}>
                        <AddIcon color='success' sx={{ fontSize: 40 }} />
                    </IconButton>
                </Tooltip>
            </Grid>
            <Grid item xs={12}>
                <List
                    sx={{
                        width: '100%',
                        maxWidth: 1000,
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 600,
                        '& ul': { padding: 0 },
                    }}>
                    {bodyRows}
                </List>
            </Grid>
            {elem != null ? (
                <>
                    {type != 'filestorage' && (
                        <Grid item xs={4}>
                            <Button
                                variant='contained'
                                sx={{ mt: 2, maxWidth: 300 }}
                                onClick={() => {
                                    setEdit(true)
                                    setDel(false)
                                }}
                                fullWidth>
                                Редактировать
                            </Button>
                        </Grid>
                    )}

                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            sx={{ mt: 2, maxWidth: 300 }}
                            onClick={() => {
                                setEdit(false)
                                setDel(true)
                            }}
                            fullWidth>
                            Удалить
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            sx={{ mt: 2, maxWidth: 300 }}
                            onClick={() => {
                                setElem(null)
                                setEdit(false)
                                setDel(false)
                                setAdd(false)
                            }}
                            fullWidth>
                            Отменить
                        </Button>
                    </Grid>
                </>
            ) : null}
            {add || edit ? (
                <>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label='Наименование'
                            value={name}
                            onChange={(event) => {
                                setName(event.target.value)
                            }}
                            sx={{ mt: 2, mb: 2, maxWidth: 900 }}
                        />
                    </Grid>

                    <Button
                        variant='contained'
                        sx={{ mb: 2 }}
                        onClick={() => {
                            if (edit)
                                axios
                                    .put(`/api/${type}/${elem.id}`, {
                                        name: name,
                                    })
                                    .then(({ data }) => {
                                        console.log(data)
                                    })
                            if (add)
                                axios
                                    .post(`/api/${type}/`, {
                                        name: name,
                                        discipline_id: discipline_id,
                                    })
                                    .then(({ data }) => {
                                        console.log(data)
                                    })
                            setEdit(false)
                            setDel(false)
                            setAdd(false)
                            setElem(null)
                            setReload(!reload)
                        }}>
                        Сохранить
                    </Button>

                    {add ? (
                        <Button
                            variant='contained'
                            sx={{ mb: 2, maxWidth: 300, ml: 2 }}
                            onClick={() => {
                                setElem(null)
                                setEdit(false)
                                setDel(false)
                                setAdd(false)
                            }}>
                            Отменить
                        </Button>
                    ) : null}
                </>
            ) : null}
            {del ? (
                <Button
                    variant='contained'
                    sx={{ mt: 2, mb: 2 }}
                    onClick={() => {
                        setEdit(false)
                        setDel(false)
                        axios
                            .delete(`/api/${type}/${elem.id}`, {})
                            .then(({ data }) => {
                                console.log(data)
                            })
                        setReload(!reload)
                    }}>
                    Подтвердите удаление
                </Button>
            ) : null}
        </>
    )
}
