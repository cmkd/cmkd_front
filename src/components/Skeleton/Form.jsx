/**  */
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import Skeleton from '@mui/material/Skeleton'

export default function(props) {
    const {
        rows = 5,
    } = props

    return Array(rows).fill(0)
        .map((_, index) => (
            <Grid key={'form' + index} item xs={12}>
                <Skeleton variant="text" width="25%" sx={{ fontSize: '.5rem' }} />
                <Skeleton variant="text" />
            </Grid>
        ))
}
