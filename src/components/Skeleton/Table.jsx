/**  */
import Skeleton from '@mui/material/Skeleton'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'

export default function (props) {
    const { rows = 5, columns = 1 } = props

    const skeletonColumns = Array(columns)
        .fill(0)
        .map((_, index) => (
            <TableCell key={'sk_col' + index}>
                <Skeleton variant="text" />
            </TableCell>
        ))

    return Array(rows)
        .fill(0)
        .map((_, index) => (
            <TableRow key={'sk_row' + index}>{skeletonColumns}</TableRow>
        ))
}
