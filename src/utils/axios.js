/**
 * Override axios error handler instance
 */
import axios from 'axios'

const instance = axios.create({
    baseURL: import.meta.env.VITE_APP_URL || '/',
    headers: {
        Authorization: 'Bearer ' + localStorage.getItem('_token'),
    },
})

instance.interceptors.response.use(null, (error) => {
    let value = null

    if (error.response) {
        const {
            data: {message},
            status,
            statusText,
        } = error.response

        if (status === 401 && window.location.pathname != '/lms/auth') {
            window.location.replace('/lms/auth')
        } else if (status !== 422) {
            value = `${status} ${statusText}`

            if (message) {
                value +=
                    ': ' + message.length > 50
                        ? message.slice(0, 50) + '...'
                        : message
            }
        }

        console.log('$ Response error:', error.response)
    } else if (error.request) {
        value = `Не удачный запрос: ${error.request.responseURL}`

        console.log('$ Response not received:', error.request)
    } else {
        value = `Ошибка в запросе: ${error.request.responseURL}`

        console.log('$ Settings error:', error.message)
    }

    return Promise.reject(error)
})

export default instance
