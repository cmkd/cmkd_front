import axios from "./axios"

export async function dfp(usereducationprofile_id, page_from, page_to, page_component, actiondfp, parameters) {
    await axios
        .post(`/api/user/dfp`, {
            usereducationprofile_id: usereducationprofile_id,
            page_from: page_from,
            page_to: page_to,
            page_component: page_component,
            actiondfp: actiondfp,
            parameters: parameters,
        })
        .then((data) => {
            console.log(data)
        })
}